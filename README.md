# UCI MKdocs site

This project is an `MKdoc` site gathering UCI group documentation.

The site was created using the instructions at:

- [Create MKdocs website](https://abpcomputing.web.cern.ch/guides/mkdocs_site/)
- [Create WebEOS project](https://how-to.docs.cern.ch/gitlabpagessite/create_site/create_webeos_project/)
- [Imported example mkdoc project](https://how-to.docs.cern.ch/gitlabpagessite/create_site/creategitlabrepo/create_with_mkdocs/#build-artifacts)

- [WebEOS site (login using webuci account)](https://webeos.cern.ch/)

The website of this documentation is located at: [uci-site.docs.cern.ch](https://uci-site.docs.cern.ch)

To get the table of content on the left hand-side to show items in order use: `01_blabla` or `02_blabla`
As soon as your push your changes the CI will run and update the website. Make sure not to break anything. Use `VScode` [`markdown preview`](https://tosbourn.com/previewing-markdown-files-with-vs-code/)

## Further documentation for mkDocs

- [User guide](https://www.mkdocs.org/user-guide/writing-your-docs/)
- [Markdown Cheat-sheet](https://markdown.land/markdown-cheat-sheet)

## To add a new section

Section are organized in subdirectories eg `docs/Analyses` or `docs/HLLHC_upgrade`.
Just create a new directory and populate it with new `.md` files.
In the `mkdocs.yml` add the new directory:

```yml
     nav_translations:
        en:
          Computing resources: Computing resources
          HLLHC upgrade: HLLHC upgrade
          Analyses: Analyses
          New Topics: New Topics
```

The filename with the actual text should be of the format `XX_Name.en.md`. `XX` is used to order the menu on the left-hand side.

## To add a new subsection

Within a `*.md` file the title should be the one of the section. The subsection will show up as a menu on the right-hand side of the webpage.

```markdown
# Introduction

## Topic-1
### Sub Topic-1
## Topic-2
```

## To publish your changes

The git repo is not yet setup to have a `development` version. Anything pushed to `master` will get push to the website within few minutes.

To make sure you don't break anything or things look as expected use the `VScode` `markdown preview enhanced` which you can add as an extension.

# Links

Here we can collect useful notes/documents that may prove useful for the general UCIrvine crew.

- All ATLAS publications can be found [here](https://twiki.cern.ch/twiki/bin/view/AtlasPublic)
- All CMS publications can be found [here](http://cms-results.web.cern.ch/cms-results/public-results/publications/)

## CERN

[CERN academic lectures](https://indico.cern.ch/category/72/)

### Misc

- [The concept of luminosity](https://cds.cern.ch/record/941318/files/p361.pdf): primer on luminosity from colliding bunches

## LHC

- [LHC Machine](https://iopscience.iop.org/article/10.1088/1748-0221/3/08/S08001/meta)
- [Standard Filling Schemes for LHC Operations](https://cds.cern.ch/record/691782/files/project-note-323_v1.pdf)
- [CERN FAQ: LHC the guide](https://cds.cern.ch/record/2255762/files/CERN-Brochure-2017-002-Eng.pdf) PDF version of pamphlet with nice FAQ

## ATLAS

- [The ATLAS FactSheet](https://cds.cern.ch/record/1457044/files/ATLAS%20fact%20sheet.pdf)
- [The ATLAS Experiment at the CERN Large Hadron Collider](https://iopscience.iop.org/article/10.1088/1748-0221/3/08/S08003/meta): Overview of the ATLAS experiment
- [TDRs](https://twiki.cern.ch/twiki/bin/view/AtlasPublic/AtlasTechnicalDesignReports): Original detector, Phase-I and Phase-II Detector & Performance
- [ATLAS Detector Run I](https://iopscience.iop.org/article/10.1088/1748-0221/3/08/S08003/pdf)
- [ATLAS Performance Run I](https://arxiv.org/abs/0901.0512)
- [ATLAS Detector Run III - Draft](https://cds.cern.ch/record/2823388/files/GENR-2019-02-001.pdf)

### General ATLAS related information

- [ATLAS Lecture Series](https://indico.cern.ch/category/17441/)

### ATLAS Sub-systems

#### Trigger/DAQ

- [HLT,DAQ,DCS TDR](https://cds.cern.ch/record/616089): slightly outdated (circa 2003)
- [The ATLAS Data Acquisition system in LHC Run 2](https://cds.cern.ch/record/2244345)
- [The ATLAS Data Acquisition and High Level Trigger system](https://iopscience.iop.org/article/10.1088/1748-0221/11/06/P06008)
- [The ATLAS Data Acquisition System: from Run 1 to Run 2](https://www.sciencedirect.com/science/article/pii/S2405601415006355)

#### ID

- [TDR](https://cds.cern.ch/record/331063?ln=en)
- [IBL TDR](https://cds.cern.ch/record/1291633)
- [The upgraded Pixel Detector of the ATLAS Experiment for Run 2 at the Large Hadron Collider](https://reader.elsevier.com/reader/sd/pii/S0168900216303837?token=4FE1AE6D61F96537799DD11BB8B869518291558E27A9F0FBA03653B6C6BDFE197DD9B3C43CD2DE4405F7A9549E979B75)
- [Slides describing general ID alignment, weak modes](http://www.hep.upenn.edu/~johnda/presentations/AtlasInnerDetectorAlignmentACAT.pdf)
- [Alignment of the ATLAS Inner Detector in Run-2](https://doi.org/10.1140/epjc/s10052-020-08700-6)

#### Muon Spectrometer

- [TDR](http://atlas.web.cern.ch/Atlas/GROUPS/MUON/TDR/Web/TDR.html)

#### Calorimeter

##### Tile

- [TDR](https://cds.cern.ch/record/331062?ln=en)

##### LAr

- [TDR](https://cds.cern.ch/record/331061?ln=en)

## Simulation

- [Simulation of Pile-up in the ATLAS Experiment](https://cds.cern.ch/record/1605834): Brief conference proceedings from Zach Marshall
- [General-purpose event generators for LHC physics](https://arxiv.org/abs/1101.2599): Andy Buckley, et al.
- [Parton distributions at the dawn of the LHC](https://arxiv.org/abs/1011.5247)
- [The PDF4LHC Working Group Interim Report](https://arxiv.org/abs/1101.0536)
- [General-purpose event generators for LHC physics](https://arxiv.org/abs/1101.2599): Detailed review of MC generators used at the LHC as well as detailed specifics on each of the generators
- [Les-Houches event format](https://arxiv.org/abs/hep-ph/0609017): explanation of the LHE file content containing event generation 

### Monte Carlo Methods

- [Introduction to parton-shower event generators](https://arxiv.org/abs/1411.4085): In depth treatment of parton showering methods in MC

### QCD Simulation in MC

- [Simulating ttbar events with additional jets at the LHC](https://www.hep.ucl.ac.uk/twiki/pub/Main/StefanRichter/TopJetsGaps_withTOC.pdf): Nice discussion on QCD, hadronization, etc with ttbar as an example (compares, e.g. Powheg methods vs Pythia)

## CP

### e/gamma

- [Electron and photon energy calibration with the ATLAS detector using 2015-2016 LHC proton-proton collision data](https://cds.cern.ch/record/2645265): ATLAS-PERF-2017-03-002
- [Electron efficiency measurements with the ATLAS detector using the 2015 data](https://cds.cern.ch/record/2157687?ln=en): Run-II note on electron reconstruction, trigger, and identification & isolation
- [Electron Reconstruction and Identification in the ATLAS Experiment using the 2015 and 2016 LHC proton-proton collision data](https://cds.cern.ch/record/2315291): Internal note, ATL-COM-PHYS-2018-436
  - [Paper, ATLAS-PERF-2017-01-001](https://cds.cern.ch/record/2633274?)
- [Measurement of the photon identification efficiencies with the ATLAS detector using LHC Run-2 data collected in 2015 and 2016](https://cds.cern.ch/record/2622453): Photon ID paper (links to internal documentation included)
- [Performance of Electron and Photon triggers in ATLAS during LHC Run 2](https://cds.cern.ch/record/2684670): V2 of paper on e/gamma triggers used in Run 2 data taking (up to and including 2018)

### Muons

- [Muon reconstruction performance of the ATLAS detector in proton-proton collision data at 13 TeV](https://arxiv.org/abs/1603.05598): Run-II note on muon reconstruction and identification working points

### Jet/Etmiss

#### Jets

- [Jet energy measurement with the ATLAS detector in proton-proton collisions at 7 TeV](https://arxiv.org/abs/1112.6426): This is _the_ paper to learn about JES/JER
- [Determination of jet calibration and energy resolution in proton-proton collisions at 8 TeV](https://cds.cern.ch/record/2298439/)

#### MET

- [Performance of missing transverse momentum reconstruction with the ATLAS detector using proton-proton collisions at 13 TeV](https://cds.cern.ch/record/2149445/)
- [MET performance in the ATLAS detector using 2015-2016 pp collisions](https://cds.cern.ch/record/2294891): MET performance CONF note
- [Object based Missing Transverse Momentum Significance in the ATLAS Detector](https://cds.cern.ch/record/2294922): MET significance CONF note

### Tau

- [Measurement of the tau lepton reconstruction and identification performance in the ATLAS experiment using pp collisions at 13 TeV](https://cds.cern.ch/record/2261772): ATLAS-CONF-2017-029

### Flavor tagging

- [ATLAS b-jet identification performance and efficiency measurement with ttbar events in pp collisions at sqrt{s} = 13 TeV](https://cds.cern.ch/record/2678410?): Paper draft of 13 TeV FTAG performance
- [Performance of b-jet Identification in the ATLAS experiment](https://arxiv.org/abs/1512.01094): Run-II flavor tagging paper, 2015
- [Optimisation and performance studies of the ATLAS b-tagging algorithms for the 2017-18 LHC run](http://cdsweb.cern.ch/record/2273281): Good overview of all Run-2 algorithms, training
- [The Soft Muon Tagger for the identification of b jets in ATLAS](https://cds.cern.ch/record/2054489?ln=en): CERN-THESIS-2015-143
- [Calibration of the ATLAS b-tagging algorithm in ttbar semi-leptonic events](https://cds.cern.ch/record/2305921): ATLAS-COM-CONF-2018-002

### Tracking and vertexing

- [Pattern recognition and estimation methods for track and vertex
  reconstruction](https://pos.sissa.it/093/003/pdf): Neat note by
  Fruhwirth, et al. Fruhwirth is a tracking boss.
- [Global Chi2 Track Fitter in
  ATLAS](http://iopscience.iop.org/article/10.1088/1742-6596/119/3/032013/meta)

## Particle Detectors, General

## Particle Physics Theory

### LHC Pheno

- [LHC Phenomenology for Physics Hunters](https://arxiv.org/abs/0810.2281): (Tilman Plehn) Quick overview of LHC pheno, QCD & scales, Hard vs Collinear Jets, Simulation, etc
- [Hard Interactions of Quarks and Gluons: a Primer for LHC Physics](https://arxiv.org/abs/hep-ph/0611148): nice overview of QCD in colliders

### Standard Model

#### Electroweak Physics

- [Electroweak Symmetry Breaking in the Standard Model](https://arxiv.org/abs/hep-ph/0503172): (Djouadi) nice treatise on the SM and EWSB/Higgs sector
- [Do charged leptons oscilate?](https://arxiv.org/abs/0706.1216)

#### QCD

- [Nambu's Nobel Prize, the $\sigma$ meson and the mass of visible matter](https://arxiv.org/abs/1403.7804): Martin Schumacher
- [Mass generation via the Higgs boson and the quark condensate of the QCD vacuum](https://arxiv.org/abs/1506.00410): Martin Schumacher (followup of the above)
- [From controversy to precision on the sigma meson](https://www.sciencedirect.com/science/article/pii/S0370157316302952): Comprehensive overview of QCD chiral symmetry breaking and the sigma meson, which is like the Higgs boson but for baryons
- [Penguin decays of B mesons](https://arxiv.org/abs/hep-ex/9804015)
- [Factorization of Hard Processes in QCD](https://arxiv.org/abs/hep-ph/0409313): Treatise on QCD factorization
- [Foundations of Perturbative QCD](https://www.cambridge.org/core/books/foundations-of-perturbative-qcd/F2869ED00FBD67B65EB7829879F3EDC4): Book by Collins (accessible with CERN license)

### BSM

#### EFT

- [Updated Global SMEFT Fit to Higgs, Diboson, and Electroweak Data](https://arxiv.org/abs/1803.03252): Global fit including latest LHC data to SMEFT and MSSM (Ellis, et al.)
- [The Standard Model as an Effective Field Theory](https://arxiv.org/abs/1706.08945): Overview of SMEFT and relevant measurements
- [Dimension-Six Terms in the Standard Model Lagrangian](https://arxiv.org/abs/1008.4884): Related to D6 operators (Warsaw basis)
- [Higgs Effective Field Theory](http://www.thphys.uni-heidelberg.de/~gk_ppbsm/lib/exe/fetch.php?media=students:lectures:student_lecture_eft.pdf):
  Introduces from a student-level Higgs Effective field theory (also references other nice things)
- [Dimension-Six Terms in the Standard Model Lagrangian](https://arxiv.org/abs/1008.4884)
- [Interpreting top-quark LHC measurements in the standard-model effective field theory](https://arxiv.org/abs/1802.07237)
- [Infrared singularities and massive fields](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.11.2856):
  Describes the "theorem" expressing the SM as a low-energy limit of a broader theory

#### Exotica

#### SUSY

- [Electroweak Symmetry Breaking in the MSSM](https://arxiv.org/abs/hep-ph/0503173): (Djouadi) nice treatise on EWSB and MSSM
- [A Supersymmetry Primer](https://arxiv.org/abs/hep-ph/9709356) : Stephen P. Martin
- [Upper bounds on supersymmetric particle masses](https://www.sciencedirect.com/science/article/pii/055032138890171X?via%3Dihub)
- [Muon anomalous magnetic dipole moment in the MSSM](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.53.6565)
- [An introduction to Supersymmetry](https://arxiv.org/abs/hep-ph/9611409): By Manuel Drees
- [Naturalness and the Status of Supersymmetry](https://arxiv.org/abs/1302.6587): Jon Feng

#### 2HDM, general

- [Theory and phenomenology of two-Higgs-doublet models](https://arxiv.org/abs/1106.0034): Overview of 2HDM

#### Dark Matter

- [Characterising dark matter searches at colliders and direct detection experiments: Vector mediators](https://arxiv.org/abs/1407.8257)
- [Direct detection of WIMPs](https://arxiv.org/abs/1002.1912)

## Cosmology/Astroparticle

- [Observational Evidence from Supernovae for an Accelerating Universe and a Cosmological Constant](https://arxiv.org/abs/astro-ph/9805201)
- [Measurements of Omega and Lambda from 42 High-Redshift Supernovae](https://arxiv.org/abs/astro-ph/9812133)
- [Planck 2015 results. XIII. Cosmological parameters](https://arxiv.org/abs/1502.01589)
- [The Eleventh and Twelfth Data Releases of the Sloan Digital Sky Survey: Final Data from SDSS-III](https://arxiv.org/abs/1501.00963)
- [Dark Energy vs. Modified Gravity](https://arxiv.org/abs/1601.06133)
- [Cosmology](http://theory.uchicago.edu/~liantaow/my-teaching/dark-matter-472/lectures.pdf): Lectures by Daniel Baumann -- good review of history of the early universe and its evolution (decoupling, freeze out, relic densities, WIMP miracle, inflation, etc...) (based on [this  course](http://cosmology.amsterdam/education/cosmology/))

### CMB

- [Cosmological Constraints on Dark Energy](https://arxiv.org/abs/1404.7266): nice & thorough explanation of what the CMB/Planck tell us and how we know the proportions of DM, DE, and regular matter from it

## Analysis Techniques

### tau tau

- [A new mass reconstruction technique for resonances decaying to tau tau](https://www.sciencedirect.com/science/article/pii/S0168900211014112?via%3Dihub): Overview of H-\>tautau reconstruction, introduces the Missing Mass Calculator (MMC) method
- [A method for identifiying H-\>tau tau -\> e mu pTmiss at the CERN LHC](https://arxiv.org/abs/hep-ph/9911385): Description of the "collinear approximation" for tau discrimination/suppression.

### mt2

- [Bisection-based asymmetric MT2 computation](https://arxiv.org/abs/1411.4312): Paper describing the now-standard MT2 calculator developed by Chris Lester with help from B. Nachman.
- [m_T2 : the truth behind the glamour](https://arxiv.org/abs/hep-ph/0304226): Good paper from the original authors of m_t2
- [MT2 / Stransverse Mass / Oxford Kinetics Library](http://www.hep.phy.cam.ac.uk/~lester/mt2/): Link to the library of mt2 algorithms and calculators by C. Lester

## Statistical Methods in HEP

- [Practical Statistics for the LHC](https://arxiv.org/abs/1503.07622): Cranmer
- [HistFitter paper](https://arxiv.org/abs/1410.1280)
- [HistFactory User's Manual](https://cds.cern.ch/record/1456844)
- [Incorporating Nuisance Parameters in Likelihoods for Multisource Spectra](https://arxiv.org/abs/1103.0354)
- [Fitting using finite Monte Carlo samples](https://www.sciencedirect.com/science/article/pii/001046559390005W?via%3Dihub): Beeston and Barlow
- [Introduction to Statistics and Data Analysis for Physicists](http://bib-pubdb1.desy.de/record/389738): Good, downloadable resource for HEP from some folks at DESY

### Limits and Significance

- [Formulae for Approximating Significance](https://cds.cern.ch/record/2643488/): Will Buttinger, report on ATLAS Statistics Committee's recommendation
- [Discovery significance with statistical uncertainty in the background estimate](https://www.pp.rhul.ac.uk/~cowan/stat/notes/SigCalcNote.pdf): Glen Cowan, Eilam Gross
- [Asymptotic formulae for likelihood-based tests of new physics](https://arxiv.org/abs/1007.1727): This is _the_ paper on asymptotic formula that we use. Cowan, Cranmer, Gross, Vitells.
- [ATLAS Frequentist Limit Recommendation](https://twiki.cern.ch/twiki/pub/AtlasProtected/StatisticsTools/Frequentist_Limit_Recommendation.pdf): Short, concise description of frequentist recipes we employ.
- [Evaluation of three methods for calculating statistical significance](https://arxiv.org/abs/physics/0702156)
- [Presentation of search results: The CL(s) technique](http://inspirehep.net/record/599622?ln=en): (Read) The reference to show for the CLs method
- [Modified frequentist analysis of search results (the CLs method)](https://cds.cern.ch/record/451614?ln=en): (Read) Another discussion on CLs

### Error Bars

- [Plotting the Differences Between Data and Expectation](https://arxiv.org/abs/1111.2062): (i.e. the ratio pad in data/MC ratio plots)
- [Recommendation for Presentation of Error Bars](http://www.pp.rhul.ac.uk/~cowan/atlas/ErrorBars.pdf): G. Cowan (2011 Stats. Forum)

## Papers/Notes

## Physics (Outside)

- [Single top-quark production at the Tevatron and the LHC](https://arxiv.org/abs/1710.10699) : Very nice (and recent, as of 2018) overview of single-top results at LHC and before

## Theses

### ATLAS Thesis Awards

#### 2019

- [Sweet Little Nothings; or, Searching for a Pair of Stops, a Pair of Higgs Bosons, and a Pair of New Small Wheels for the Upgrade of the Forward Muon System of the ATLAS Detector at CERN](https://cds.cern.ch/record/2699575?ln=en): Daniel Joseph Antrim
- [Electroweak Physics at the Large Hadron Collider with the ATLAS Detector: Standard Model Measurement, Supersymmetry Searches, Excesses,and Upgrade Electronics](https://cds.cern.ch/record/2684038?ln=en): Elodie Deborah Resseguie
- [Measurement of Higgs boson production cross sections in the diphoton channel with the full ATLAS Run-2 data and constraints on anomalous Higgs boson interactions](https://cds.cern.ch/record/2696211?ln=en): Ahmed Tarek
- [Search for long-lived, massive particles in events with a displaced vertex and a displaced muon using \\sqrt{s} = 13 TeV pp-collisions with the ATLAS detector](https://cds.cern.ch/record/2677474?ln=en): Karri Folan Di Petrillo
- [Seeing the Light (Higgs): Searches and Measurements of Higgs Boson Decays to Photons](https://cds.cern.ch/record/2676822?ln=en): Khilesh Pradip Mistry
- [Higgs cross section measurements at \\sqrt{s} = 13 TeV using the ATLAS detector](https://cds.cern.ch/record/2697418?ln=en): Stephen Burns Menary

#### 2018

- [Search for the Production of a Standard Model Higgs Boson in Association with Top-Quarks and Decaying into a Pair of
  Bottom-Quarks with 13 TeV ATLAS Data](https://cds.cern.ch/record/2320703?ln=en): Nedaa Alexandra Asbah
- [b-Tagging and Evidence for the Standard Model H→bb¯ Decay with the ATLAS Experiment](https://cds.cern.ch/record/2644018?ln=en): Andrew Bell
- [Top quark pair production measurements in the single lepton channelusing the ATLAS detector](https://cds.cern.ch/record/2644103?ln=en): Rafal Bielski
- [Searches for the Supersymmetric Partner of the Top Quark, Dark Matter and Dark Energy at the ATLAS Experiment](https://cds.cern.ch/record/2637040?ln=en): Nicolas  Kohler
- [Physics with photons with the ATLAS Run 2 data: calibration and identification, measurement of the Higgs boson mass and search for
  supersymmetry in di-photon final state.](https://cds.cern.ch/record/2303078?ln=en): Stefano Manzoni
- [Studies of adhesives and metal contacts on silicon strip sensors for the ATLAS Inner Tracker](https://cds.cern.ch/record/2320823?ln=en): Anne-Luise Poley

#### 2016

- [Studies of radiation damage in silicon sensors and a measurement of the inelastic proton--proton cross-section at 13 TeV](https://cds.cern.ch/record/2227180): Miguel Arratia
- [Physics with Electrons in the ATLAS Detector](https://cds.cern.ch/record/2228644): Kurt Brendlinger
- [Search for new phenomena in dijet angular distributions at √s = 8 and 13 TeV](https://cds.cern.ch/record/2131851): Lene Bryngemark
- [Observation and measurement of the Higgs boson in the WW∗ decay channel with ATLAS at the LHC](https://cds.cern.ch/record/2134470): Joana Machado Miguéns
- [Investigating the Quantum Properties of Jets and the Search for a Supersymmetric Top Quark Partner with the ATLAS Detector](https://cds.cern.ch/record/2204912): Ben Nachman
- [Mesure de la section efficace de production de paires de photons isolés dans l'expérience ATLAS au LHC et étude des couplages à quatre photons](https://cds.cern.ch/record/2198280): Matthias Saimpert

#### 2015

- [Search for new physics in ttbar final states with additional heavy-flavor jets with the ATLAS detector](https://cds.cern.ch/record/2053769?ln=en): Javier Montejo Berlingen
- [Search for Dark Matter in events with a highly energetic jet and missing transverse momentum in proton-proton collisions at s√ = 8 TeV with the ATLAS Detector](https://cds.cern.ch/record/2016807?ln=en): Ruth Pöttgen
- [Search for Standard Model H→τ+τ− Decays in the Lepton-Hadron Final State in Proton-Proton Collisions with the ATLAS Detector at the LHC](https://cds.cern.ch/record/1984325?ln=en): Nils Ruthmann
- [Searching for Dark Matter with the ATLAS Detector in Events with an Energetic Jet and Large Missing Transverse Momentum](https://cds.cern.ch/record/2014029?ln=en): Steven Schramm

### UCI superstars

- [Sweet Little Nothings; or, Searching for a Pair of Stops, a Pair of Higgs Bosons, and a Pair of New Small Wheels for the Upgrade of the
  Forward Muon System of the ATLAS Detector at CERN](https://cds.cern.ch/record/2699575?ln=en): Daniel Joseph  Antrim
- [Search for the stau slepton at DELPHI and muon identification and Z0 production at CDF](https://cds.cern.ch/record/619089?ln=en):
  Anyes Taffard
- [Searches for Supersymmetry in Multilepton Final States with the ATLAS Detector](https://cds.cern.ch/record/1645492?ln=en): Steve Farrell
- [Search for a heavy charged gauge boson decaying to a muon and a neutrino in 1 fb-1 of proton-proton collisions at (√s) = 7 TeV using the ATLAS Detector](https://cds.cern.ch/record/1454661?ln=en): Serhan Mete
- [Searches for Heavy Neutrinos and Supersymmetry in Dilepton Events at the ATLAS Experiment](https://cds.cern.ch/record/1648261?ln=en): Matthew Relich
- [Measurement of the differential cross-section with respect to momentum and rapidity of the J/ψ meson and the non-prompt J/ψ
  cross-section fraction in pp collisions at sqrt(s)=7 TeV with the ATLAS detector](https://cds.cern.ch/record/1390515?ln=en): Andy
  Nelson
- [Search for Dark Matter in Missing-Energy Final States with an Energetic Jet or Top Quarks with the ATLAS Detector](https://cds.cern.ch/record/2268281?ln=en): Johanna Gramling
- [Azimuthally Sensitive Hanbury Brown–Twiss Interferometry measured with the ALICE Experiment](https://cds.cern.ch/record/1463351/files/CERN-THESIS-2012-088.pdf): Johanna Gramling (Masters, ALICE)
- [Performance characterization of the Micromegas detector for the New Small Wheel upgrade and Development and improvement of the Muon Spectrometer Detector Control System in the ATLAS experiment](https://cds.cern.ch/record/2143887?ln=en): Kostas Ntekas

### Miscellaneous

- [A search for massive particles decaying into multiple quarks with the ATLAS detector at the LHC](https://cds.cern.ch/record/2059327?ln=en) Larry Lee
- [The Road to Discovery : Detector Alignment, Electron Identification, Particle Misidentification, WW Physics, and the Discovery of the Higgs Boson](https://cds.cern.ch/record/1536507?ln=en): John Alison
- [Searches for exotic stable massive particles with the ATLAS experiment](https://cds.cern.ch/record/1397376/): Christian Ohm
- [Identification of b-jets and investigation of the discovery potential of a Higgs boson in the WH-\>lvbb channel](https://cds.cern.ch/record/1243771/files/CERN-THESIS-2010-027.pdf): Giacinto Piacquadio

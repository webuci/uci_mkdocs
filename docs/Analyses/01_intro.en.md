# Introduction

Find under this section, the list of analyses and some pointers about ATLAS software or useful documentation related to analyses.

Our UCI analysis framework documentation can be found under the [antNT twiki](https://gitlab.cern.ch/groups/anteater/-/wikis/home)

- [Run-III/IV Analysis Prospects Google Doc](https://docs.google.com/document/d/1p7NYSpwUPtMsZSjtrcISOvYE57Ea7CoUaimxf662hC0/edit?usp=sharing)
- [UCI gitlab group](https://gitlab.cern.ch/anteater)

## ATLAS Software

Good place to start are the ATLAS [tutorials](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorial) and go through the [Tutorial Week slides & videos](https://atlassoftwaredocs.web.cern.ch/ASWTutorial/TutorialWeek/) (pick the latest one).
The [ATLAS Software](https://atlassoftwaredocs.web.cern.ch/) documentation page has multiple guides and tutorials.

Additional UCI tips can be found [here](https://gitlab.cern.ch/groups/anteater/-/wikis/General/ATLAS-software-setup)

### Grid access

General [instructions to get started with Grid](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/WorkBookStartingGrid)

- [Get a Grid certificate](https://atlassoftwaredocs.web.cern.ch/ASWTutorial/basicSetup/grid_vo/)
- [Joining ATLAS VO](https://www.sdcc.bnl.gov/experiments/usatlas/joining-atlas-vo)
- [Obtain/renew Grid certificate](https://ca.cern.ch/ca/Default.aspx)
  - [Upload grid certificate to browser](https://ca.cern.ch/ca/Help/) and [here](https://ca.cern.ch/ca/Help/?kbid=070110)

## GIT

All ATLAS code seats under [GitLab](https://gitlab.cern.ch/)

- List of [Releases](https://gitlab.cern.ch/atlas/athena/tree/21.2). Change release branch as needed.
- [Releases Twiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisRelease)

To browse code use [LXR](https://acode-browser.usatlas.bnl.gov/lxr/source)

## Making Feynman Diagrams

The SUSY group provide a handy latex tools to do this:
<https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYFeynmanDiagrams>

We also have an [overleaf version](https://www.overleaf.com/project/628e45e0d5f27075344e3900) (request access).

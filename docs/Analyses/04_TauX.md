# Tau + X

Useful resources for Tau + X analysis: 

- [ANA-EXOT-2022-34](https://atlas-glance.cern.ch/atlas/analysis/analyses/details?id=9039)
- [Code repository on Git](https://gitlab.cern.ch/atlas-phys/exot/lpx/exot-2022-34/)
- [Indicomb page for analysis meetings](https://ilongari.web.cern.ch/indicomb/TauX.html)


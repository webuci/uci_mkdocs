# Software for analysis

## Software available from CVMFS

### ATLAS and CERN software
Most of this software is setup via `cvmfs`. 

You can add these commands to your `~/.bash_profile` file in order:
```bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
```
Normally most of the software needed to do analysis work (e.g. Athena, AnalysisBase, ROOT) can be setup via `lsetup` or `asetup` commands. These two commands will become available after you do the `setupATLAS` command, which is enabled by the two lines above.

A quick way to setup ROOT is:
```bash
setupATLAS
lsetup "root recommended"
```

### LCG and other packages
Sometimes you might need other software, for python it is always recommended to install packages under a virtual environment. Check the [venv](https://docs.python.org/3/library/venv.html) command or [pyenv](https://github.com/pyenv/pyenv). The former is just a virtual environment creation module, the latter allows you to have several python installations available.

The LCG software releases also include a version of Python and several other packages. Check [lcginfo.cern.ch](https://lcginfo.cern.ch) for more information. In order to setup a LCG release, after doing `setupATLAS` you can do a command like this:
```
lsetup "views LCG_106 x86_64-el9-gcc13-opt"
```

### More information on software for analysis
[Check the ATLAS Software Tutorial](https://atlassoftwaredocs.web.cern.ch/analysis-software/ASWTutorial/)


## Local installation
Please find below some recommendations of software to install if you need to perform analysis tasks on your laptop/workstation.

###  Windows users
If you are under Windows you cannot install ROOT or conda.
You can have `conda` and `python`, but `Windows` is not the best supported OS for `ROOT`.

There are three common solutions for this.
- Install a linux distribution on your laptop with dual boot (e.g. following [this guide](https://itsfoss.com/install-ubuntu-1404-dual-boot-mode-windows-8-81-uefi/))
    - Maximum compatibility, as you will run directly Linux on your laptop
    - A lot of free storage space is required
    - Sometimes removing a dual-boot installation can be tricky
- Install a virtual machine using [virtualbox](https://www.virtualbox.org)
    - Less intrusive, as the virtual machine running the linux OS will run under your windows installation. 
    - Normally requires a powerful PC
- Install the Windows Subsystem for Linux WSL, which allows you to run a linux terminal directly in windows. Instructions can be found [here](https://docs.microsoft.com/en-us/windows/wsl/install)
    - Probably the best option for basic tasks, but it is available only on recent versions (Windows 10 minimum)

Once  you have the linux VM or OS installed follow the corresponding instructions below.

### MAC users
The xcode developers package is a common requirement. You will need to open your terminal and type
```
xcode-select --install
```
This will provide a python executable and C/C++ compilers. 
macOS does not provide a package manager, a recommended one is [brew](https://brew.sh). This will allow you to install python and ROOT and potentially many other UNIX packages.

#### Python/ROOT installation using brew
To install python
```
brew install python
```

To install python packages
```
python -m pip install <<packagename>>
```

```bash
brew install root  
```
At the end of the installation brew will give you instructions on how to enable ROOT in your session. This is normally done by running `source` on a file named `thisroot.sh`. 


#### Python/ROOT installation using Anaconda
You can/should install anaconda under your home area, so you can setup python environment across the various UCI nodes
To install environment with CERN Root libraries, see [detailed instructions here](https://iscinumpy.gitlab.io/post/root-conda/)

```bash
conda create -n my_root_env root -c conda-forge
conda activate my_root_env
conda config --env --add channels conda-forge
```

If you get an error trying to install `conda`, specify the python version.

```bash
conda create -n <env> python=3.9.10
conda activate <env>
conda install -c conda-forge root
```

Normally, one need to have in the bash shell something like `setupConda` to activate conda:

```bash
function setupConda(){
    source $HOME/miniconda/etc/profile.d/conda.sh
    alias condaroot='conda activate my_root_env'
    echo "Setting Conda"
}
```

Each time you want to work in the `conda` environment:

```bash
setupConda
condaroot
```

If you need to remove the environment and start from scratch:

```bash
conda deactivate
conda env remove -n <env>
```

To list all the packages installed in the environment:

```bash
conda list
```

#### Checking python/ROOT installation
At this point you should make sure that ROOT can be imported in python by running the interactive python shell and doing:
```python3
import ROOT
```
If you get no error, then you have all what’s needed.



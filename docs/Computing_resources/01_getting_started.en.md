# Getting started
In the High Energy Physics (HEP) community all the analysis tasks are performed by running code using the linux command line. Most of the analysis code will be either Python or C++. 

These languages are widely used due to their flexibility, performance, and strong support for scientific computing. Python is often favored for prototyping, data manipulation, and high-level analysis. C++, on the other hand, is commonly used for performance-critical tasks, such as event processing and simulation, particularly within frameworks like ROOT.

## Basic requirements
In order to get started it is required to be familiar with the linux command line. 
This command line is already available if you are working on a linux or Mac computer. If you are on windows you can follow [these instructions](./03_softwares.en.md#local-installation) in order to install a linux shell.

An introductory guide to the linux command line can be found [here](https://ubuntu.com/tutorials/command-line-for-beginners).

Some knowledge of Python and C++ is required. There are several resources available on the internet, as well as books that that explain these languages on different levels.

[ROOT](http://root.cern) is the main HEP analysis framwork. It allows to store complex data structures in files, process them iteratively and perform a statistical analysis on them. Its plotting library is the standard used by the HEP community. 

While most of the work could be theoretically performed on a laptop, it is generally better to run analysis tasks on dedicated servers that are much more powerful and have access to large storage and network file systems. Beside this, it is still helpful to install python and ROOT on your computer, in order to quickly run some tests and plotting offline. Instructions on how to do this can be found in [this page](./03_softwares.en.md).

## Working on remote computers
### ssh
The ssh command is used in order to access remote computers. [`ssh`](https://linux.die.net/man/1/ssh) is normally installed in all linux distributions and can be used also in the [windows terminal](https://learn.microsoft.com/en-us/windows/terminal/tutorials/ssh).
If you have an account with username `username` on the computer named `server`, then you can `ssh` with:
```bash
ssh username@server
```

Commands that are run on that remote computer might result in files (e.g. pdf files of plots) that you need to copy to your local laptop. For this purpose you can use the [`scp`](https://linux.die.net/man/1/scp) command.

In order to avoid using a password every time you do `ssh` or `scp`, you can setup a password-less authentication following [these instructions](./05_miscellaneous.en.md#password-less-authentication-using-ssh).


### Remote desktop and remote windows
If the command that you run on the remote server will result in a window being open on the desktop, normally you will not have access to this unless you add `-XY` as arguments to your ssh commands. Doing `ssh -XY username@server` will allow to forward any GUI to your local computer (you will need to install [XQuartz on a mac](https://www.xquartz.org) or follow [these instructions on Windows](https://learn.microsoft.com/en-us/windows/wsl/tutorials/gui-apps)). However, this method is very slow especially if the remote machine is not in the same network as yours.

Sometimes you may need access to the remote desktop of that server. This can be performed using x2go, which performs a lot better with respect to the X window forwarding done by `ssh -XY`. Instructions on how to setup x2go can be found [here](./04_tools.en.md#remote-access-via-x2go). 
Please note that a x2go server is not installed by default in all linux system, while the X window forwarding is generally available.

## Computing clusters avialable
The following computing clusters are available. Click on the links to get more info.
 - [UCI Cluster](./02_cluster.en.md#uci-cluster)
 - [CERN LXPLUS](./02_cluster.en.md#cern-lxplus)
 - [UChicago Analysis Facility](./02_cluster.en.md#us-atlas-analysis-facilities)


## Analysis software
All the computer clusters cited above have access to CERN CVMFS, which gives the possibilty of setting up software like ROOT, Athena, Python, etc. More information can be found [here](./03_softwares.en.md#software-available-from-cvmfs)

## Editing files remotely
There are two options for editing files remotely. The most convenient approach nowadays is to use [Visual Studio Code](https://code.visualstudio.com/Download), which allows to connect to a remote server, view files, access remote terminal, all in the same IDE.
More instructions on how to setup VSCode and a convenient set of extensions can be found [here](./04_tools.en.md#vscode). 

Sometimes you might need to do quick changes on the fly when connected to a remote computer via ssh. For this purpose you can learn how to use a simple editor that can be run on the command line like [emacs](https://www.gnu.org/software/emacs/manual/html_node/emacs/index.html), [vim](https://www.linuxfoundation.org/blog/blog/classic-sysadmin-vim-101-a-beginners-guide-to-vim) or [nano](https://www.nano-editor.org/dist/v2.2/nano.html).






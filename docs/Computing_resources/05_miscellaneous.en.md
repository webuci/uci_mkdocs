# Miscellaneous

- [US ATLAS shared computing resources docs](https://usatlas.readthedocs.io/projects/af-docs/en/latest/)
- [Using containers in ATLAS with ATLASLocalRootBase/setupATLAS](https://twiki.atlas-canada.ca/bin/view/AtlasCanada/Containers)


## Password-less authentication using ssh
Since you will be accessing remote servers multiple times in a day, it is highly recommended that you setup a password-less authentication.

Two methods are described below, the keypair-based method is universal and works on nearly all servers. The GSSAPI method is required for LXPLUS.

### keypair-based authentication
Run the following command on your local machine to create an SSH key pair:
```
ssh-keygen -t rsa -b 4096 
```
Press Enter to save in the default location (~/.ssh/id_rsa).

Add the key to the remote server:
```
cat ~/.ssh/id_rsa.pub | ssh user@remote_server "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"
```

Ensure the correct file permissions on the remote server, login on the server first and do:
```
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
```
### GSSAPI authentication
These instructions are required for CERN LXPLUS cluster.

On your machine, add these lines to the `~/.ssh/config` file:
```
Host lxplus
     User yourusername
     HostName lxplus.cern.ch
     ServerAliveInterval 120
     TCPKeepAlive no
     ForwardX11 yes
     ForwardX11Trusted yes
     GSSAPIAuthentication yes
     GSSAPIDelegateCredentials  yes
```

The two lines with `ForwardX11` and `ForwardX11Trusted` automatically enable the `-XY` options of `ssh`. 

The authentication works if you have a valid kerberos token. To generate it you need the following command:
`kinit -l 20h yourusername@CERN.CH`.  
If you don't get any error you're good to go. To connect to LXPLUS you need to tell the ssh service to use this token. If you have set up the alias as above then you can simply use
`ssh lxplus`, and it should work without asking you anything.  

If you don't want the alias, or maybe you want to connect to a specific machine you can pass the option `-K` to ssh. Something like `ssh -K yourusername@lxplus111.cern.ch`.  

Passwordless file copy via `scp` also works with this method, but only if you create the ssh alias.

The kerberos token expires after a given time, if you don't specify `-l 20h` to the command `kinit`, then it will last 12 hours. The maximum allowed by LXPLUS is less than 24h, so you will need to do that once a day.  

If you are on a mac, you can also save your password in the system keychain. For this you can do (just once)
`kinit --keychain yourusername@CERN.CH` and then it will ask for your cern account password just once. Afterwards you will only need to type `kinit` once a day.



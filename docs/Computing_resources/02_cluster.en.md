# Computing clusters

## UCI Greenplanet cluster
The UCI computing cluster is composed of the following machines under the `.ps.uci.edu` domain:

- `uclhc-1` [Taffard' group. For analyses work]
- `uclhc-2` [Taffard' group. For HL-LHC upgrade work]
- `gpatlas1` [Taffard' & Whiteson' groups. For analyses work]
- `gpatlas2` [Taffard' & Whiteson' groups. For analyses work]
- `atlas-gpu` [Taffard' & Whiteson' groups. For ML work]

These compute login nodes are part of the Physical Science GreenPlanet ([`hpcops@ps.uci.edu`](mailto:hpcops@ps.uci.edu)) cluster located at the Engineering Gateway.
To login to these nodes, you'll need to either be on the UCI network of use UCI VPN, and `ssh` to the node.
The user `home area` is shared across the nodes.

[HTCondor](https://htcondor.readthedocs.io/en/latest/grid-computing/index.html) batch jobs can be submitted from these nodes to run parallel job on the grid.
The [UCI brick Monitoring](https://atlas-uci.web.cern.ch/atlas-uci/atlas-uci-brick-monitoring) webpage provides a simple monitoring of each node.

### Getting an account
Email Prof. Taffard to get an account.
If you do not have a UCINetID, we will need to create a sponsored UCINetID.

### Access from outside campus
Access from off-campus will probably need to use the VPN.
See [these instrcutions](https://www.oit.uci.edu/services/security/vpn) for installation and configuration.

CERN IPs are whitelisted and can access these computers directly. If you are in Europe you can add `-J yourusername@lxtunnel.cern.ch` in order to jump over LXPLUS to connect. However, this option will make the connection slower if you are in the US.

### Software
Some scientific software packages are installed in the network mounted directory `/sopt`, and are set up through environment modules.
To see a list, type:
```bash
ml spider
```

and load them with:
```bash
ml package/version
```

You can also look [here](https://ps.uci.edu/greenplanet/Software) for more info.

Email [hpcops AT ps.uci.edu](mailto:hpcops@ps.uci.edu) for help!

### File storage at UCI
This section is valid for the systems managed by GreenPlanet.

User home directories are backed up, but limited to `20 GB`.

As a general rule of thumb avoid storing large files (e.g. analysis ntuples) in the home directory. There are several data drives that are here for this purpose.

#### Data drives
For more space (e.g. downloading n-tuples) each machine has a local storage that can be accessed by `/machine_name/data` (e.g. `/uclhc-1/data/`). All drives are cross-mounted via the network and should be normally accessible from other computers.

#### GreenPlanet drives (do not use!)
Eventually, more space is available in the GreenPlanet drives:
- `/XXX-L/SCRATCH/taffard/<username>`
- `/DFS-L/DATA/taffard/<username>`
This drive is a GreenPlanet drive and storage results in charges to the ATLAS budget. You should avoid storing data on this drive, and instead use the many TB available on the local SATA drives.

#### SSDs
`uclhc-2` has 2 SSD drives, mounted at `/fast` and `/quick`. These drives should be used to develop analysis code, especially if you need to compile, since that's much faster on SSD. Do not keep large files since those drives are only 1.8 TB each.


#### File sharing
Your work area should be readable by other members of the group since we often need access to each other areas.

For the home directory this should be already the default. For folders under the SSDs and the Data drives, you can run the following command (on uclhc-1 for example).
```bash
cd /uclhc-1/data/uclhc/uci/user/ 
mkdir $(whoami)
chmod a+rx ataffard
```

Both under `uclhc-1` and `uclhc-2`, we have a `ucigroup` area that we use to stored shared files (e.g. `ESD`, `xAOD`, `ROOT` ntuple, `hdf5` etc...):
```bash
/uclhc-1/data/uclhc/uci/user/ucigroup
/uclhc-2/data/uclhc/uci/user/ucigroup/
```
Please store any large files there and change the permissioon to the directory with:
```
chmod g+rw <dir>
``` 

#### Setting bash scripts
Feel free to grab `ataffard` bash scripts and modify as you see fit:
- `.bashrc`
- `.bash_profile`
- `.bash_profile_atlas-gpu`
- `.bash_profile_gpatlas`
- `.bash_profile_uclhc-1`
- `.bash_profile_uclhc-2`
- `.bash_prompt`

Check for any username (`ataffard`) and replace with yours.
There are also few useful scripts under `~ataffard/AtlasSetup`

Look into each of these `.bash*` files to be aware of what they do and the aliases and functions provided

## UCI ATLAS lab computers
For HL-LHC upgrade trigger project, the UCI ATLAS lab has the following computers under the `.ps.uci.edu` domain. These machines are not managed by GreenPlanet, in order to request access please contact Prof. Taffard.

- `uciatlaslab`: used for the MDTTP trigger project (some doc in this [git repo](https://gitlab.cern.ch/anteater/uciatlaslab))
- `ucieftrk`: used for the EFTrk trigger project

These machines are intended to be used for Phase-2 upgrade work. They do not implement 

These machines have restricted remote access. UCI VPN IP addresses and CERN IPs are allowed. If needed, additional IP addresses can be added to the whitelist.


## CERN LXPLUS
LXPLUS is the main CERN linux cluster. You will need a CERN account in order to access it. Please note that this cluster is shared with many users and normally can be slower than the UCI cluster for daily use.
For more information:
- [lxplus ssh help](https://twiki.cern.ch/twiki/bin/view/LinuxSupport/SSHatCERNFAQ)
- [Kerberos Access](https://linux.web.cern.ch/docs/kerberos-access/)

## US-ATLAS analysis facilities
These facilities are available for US-ATLAS members.

Check the [documentation](https://usatlas.readthedocs.io/projects/af-docs/en/latest/) in order to get an account.

The UChicago AF normally allows ATLAS users working on analysis to get an account. It offers fast access to the MWT2 data disk
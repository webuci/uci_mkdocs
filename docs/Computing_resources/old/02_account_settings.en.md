# Account settings and storage

## Login to a cluster node
You should be familiar with the linux terminal commands. An introductory guide can be found  [here](https://ubuntu.com/tutorials/command-line-for-beginners#4-creating-folders-and-files).

The ssh command in order to access remote servers. `ssh` is normally installed in all linux distributions and can be used also in the [windows terminal](https://learn.microsoft.com/en-us/windows/terminal/tutorials/ssh).
```bash
ssh -XY username@uclhc-1.ps.uci.edu
passwd
```

Use "passwd" to change your initial password. Password changes take up to 5 minutes to propagate.

Access from off-campus will probably need to use the VPN.
See [these instrcutions](https://www.oit.uci.edu/services/security/vpn) for installation and configuration.

### Software
Generic scientific software packages are installed in the network mounted directory `/sopt`, and are set up through environment modules.
To see a list, type:
```bash
ml spider
```

and load them with:
```bash
ml package/version
```

You can also look [here](https://ps.uci.edu/greenplanet/Software) for more info.

Email [hpcops AT ps.uci.edu](mailto:hpcops@ps.uci.edu) for help!

## File storage at UCI
This section is valid for the systems managed by GreenPlanet.

User home directories are backed up, but limited to `20 GB`.

As a general rule of thumb avoid storing large files (e.g. analysis ntuples) in the home directory. There are several data drives that are here for this purpose.
Analysis code should also go under one of the data drive and backup to CERN gitLab, preferentially under [Anteater GitLab area](https://gitlab.cern.ch/anteater).

### Data drives
For more space (e.g. downloading n-tuples) each machine has a local storage that can be accessed by `/machine_name/data` (e.g. `/uclhc-1/data/`). All drives are cross-mounted via the network and should be normally accessible from other computers.

### GreenPlanet drives (do not use!)
Eventually, more space is available in the GreenPlanet drives:
- `/XXX-L/SCRATCH/taffard/<username>`
- `/DFS-L/DATA/taffard/<username>`
This drive is a GreenPlanet drive and storage results in charges to the ATLAS budget. You should avoid storing data on this drive, and instead use the many TB available on the local SATA drives.

### SSDs
`uclhc-2` has 2 SSD drives, mounted at `/fast` and `/quick`. These drives should be used to develop analysis code, especially if you need to compile, since that's much faster on SSD. Do not keep large files since those drives are only 1.8 TB each.


### File sharing
Your work area should be readable by other members of the group since we often need access to each other areas.

For the home directory this should be already the default. For folders under the SSDs and the Data drives, you can run the following command (on uclhc-1 for example).
```bash
cd /uclhc-1/data/uclhc/uci/user/ 
mkdir $(whoami)
chmod a+rx ataffard
```

Both under `uclhc-1` and `uclhc-2`, we have a `ucigroup` area that we use to stored shared files (e.g. `ESD`, `xAOD`, `ROOT` ntuple, `hdf5` etc...):
```bash
/uclhc-1/data/uclhc/uci/user/ucigroup
/uclhc-2/data/uclhc/uci/user/ucigroup/
```
Please store any large files there and change the permissioon to the directory with:
```
chmod g+rw <dir>
``` 


## Setting bash scripts
Feel free to grab `ataffard` bash scripts and modify as you see fit:
- `.bashrc`
- `.bash_profile`
- `.bash_profile_atlas-gpu`
- `.bash_profile_gpatlas`
- `.bash_profile_uclhc-1`
- `.bash_profile_uclhc-2`
- `.bash_prompt`

Check for any username (`ataffard`) and replace with yours.
There are also few useful scripts under `~ataffard/AtlasSetup`

Look into each of these `.bash*` files to be aware of what they do and the aliases and functions provided

## ATLAS and CERN software
Most of this software is setup via `cvmfs`. 

You can add these commands to your `~/.bash_profile` file in order:
```bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
```
Normally most of the software needed to do analysis work (e.g. Athena, AnalysisBase, ROOT) can be setup via `lsetup` or `asetup` commands. These two commands will become available after you do the `setupATLAS` command, which is enabled by the two lines above.

A quick way to setup ROOT is:
```bash
setupATLAS
lsetup "root recommended"
```

Sometimes you might need other software, for python it is always recommended to install packages under a virtual environment. Check the [venv](https://docs.python.org/3/library/venv.html) command or [pyenv](https://github.com/pyenv/pyenv). The former is just a virtual environment creation module, the latter allows you to have several python installations available.

The LCG software releases also include a version of Python and several other packages. Check [lcginfo.cern.ch](https://lcginfo.cern.ch) for more information. In order to setup a LCG release, after doing `setupATLAS` you can do a command like this:
```
lsetup "views LCG_106 x86_64-el9-gcc13-opt"
```


## Remote access via X2Go
Sometime one need to open a GUI on `uclhc-1` or `uclhc-2`. X11 on its own is too slow, and it is recommended to use [X2Go client](https://wiki.x2go.org/doku.php).
Install the client on your computer.
Open the `X2Go` client application and add a new profile with the following settings:

![X2Go Session tab](https://uci-site.docs.cern.ch/pics/X2GoSettings_1.png "Session tab"){ width=60%,height:30px }
This is setup to use `ssh-key`. If you don't have `ssh-key` setup, un-tick the login via `ssh`
![X2Go Input/output tab](https://uci-site.docs.cern.ch/pics/X2GoSettings_2.png "Input/output tab"){ width=60%,height:30px }
![X2Go Media tab](https://uci-site.docs.cern.ch/pics/X2GoSettings_3.png "Media tab"){ width=60%,height:30px }
![X2Go Shared folders tab](https://uci-site.docs.cern.ch/pics/X2GoSettings_4.png "Shared folders tab"){ width=60%,height:30px }

Once login, you'll get a linux-like desktop window. Open a terminal and setup ATLAS software to open a ROOT file for e.g.

## Connecting to CERN LXPLUS
LXPLUS is the main CERN linux cluster. You will need a CERN account in order to access it. Please note that this cluster is shared with many users and normally can be slower than the UCI cluster for daily use.
For more information:
- [lxplus ssh help](https://twiki.cern.ch/twiki/bin/view/LinuxSupport/SSHatCERNFAQ)
- [Kerberos Access](https://linux.web.cern.ch/docs/kerberos-access/)

## US-ATLAS analysis facilities
These facilities are available for US-ATLAS members.

Check the [documentation](https://usatlas.readthedocs.io/projects/af-docs/en/latest/) in order to get an account.

## More information on software for analysis
[Check the ATLAS Software Tutorial](https://atlassoftwaredocs.web.cern.ch/analysis-software/ASWTutorial/)

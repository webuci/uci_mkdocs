# UCI Cluster

The UCI computing cluster is composed of the following machines under the `.ps.uci.edu` domain:

- `uclhc-1` [Taffard' group. For analyses work]
- `uclhc-2` [Taffard' group. For HL-LHC upgrade work]
- `gpatlas1` [Taffard' & Whiteson' groups. For analyses work]
- `gpatlas2` [Taffard' & Whiteson' groups. For analyses work]
- `atlas-gpu` [Taffard' & Whiteson' groups. For ML work]

These compute login nodes are part of the Physical Science GreenPlanet ([`hpcops@ps.uci.edu`](mailto:hpcops@ps.uci.edu)) cluster located at the Engineering Gateway.
To login to these nodes, you'll need to either be on the UCI network of use UCI VPN, and `ssh` to the node.
The user `home area` is shared across the nodes.

[HTCondor](https://htcondor.readthedocs.io/en/latest/grid-computing/index.html) batch jobs can be submitted from these nodes to run parallel job on the grid.
The [UCI brick Monitoring](https://atlas-uci.web.cern.ch/atlas-uci/atlas-uci-brick-monitoring) webpage provides a simple monitoring of each node.

## Getting an account
Email Prof. Taffard to get an account.
If you do not have a UCINetID, we will need to create a sponsored UCINetID.

## UCI ATLAS lab computers
For HL-LHC upgrade trigger project, the UCI ATLAS lab has the following computers under the `.ps.uci.edu` domain. These machines are not managed by GreenPlanet, in order to request access please contact Prof. Taffard.

- `uciatlaslab`: used for the MDTTP trigger project (some doc in this [git repo](https://gitlab.cern.ch/anteater/uciatlaslab))
- `ucieftrk`: used for the EFTrk trigger project

These machines have restricted remote access. UCI VPN IP addresses and CERN IPs are allowed. If needed, additional IP addresses can be added to the whitelist.
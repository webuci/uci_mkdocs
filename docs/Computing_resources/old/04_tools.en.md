# Tools

Below are some tools widely used in HEP-EX

## Electronics logbook

Keeping a logbook is important to organize your work, thoughts and planning.  
Here are few options for electronics options, which are easy to use, and that you can search !

- [Obsidian](https://obsidian.md/)
- [One Note](https://www.microsoft.com/en-us/microsoft-365/onenote/digital-note-taking-app?ms.url=onenotecom&rtc=1)
  - [Access via UCI MS365 account](https://www.oit.uci.edu/services/end-point-computing/microsoft-365/)
- [CERN codimd](https://codimd.web.cern.ch/)

To keep todo list:

- [Microsoft TODO](https://www.microsoft.com/en-us/microsoft-365/microsoft-to-do-list-app)
- [MAC reminder](https://apps.apple.com/us/app/reminders/id1108187841) 
  - You can drop emails to make then as todo's

## Command line

- [tmux](https://tmux.github.io) – multiplex several terminal sessions/persistent session also when ssh connection drops
- [vim](https://www.vim.org) – good editor ([cheat sheet/quick reference](http://tnerual.eriogerg.free.fr/))
- [vimtex](https://github.com/lervag/vimtex.git) – VIM plugin to ease writing LaTeX documents


## Code editor

Excellent editor for coding:

[Visual Studio Code](https://code.visualstudio.com/Download). It’s free.
Install some `VSCode` [extensions](https://livecodestream.dev/post/best-vscode-extensions-for-python/)

Then follow these [instructions](https://code.visualstudio.com/docs/remote/ssh) to do a remote ssh to the UCI nodes.
`VScode` has an integrated terminal that is very useful. One should be able to do pretty much everythign from `VSCode` with all code and files on the UCI cluster.

There a lot of very useful extension for `VScode`, here are some that we widely use:

- Basic Extension
- Remote ssh and Remote X11 (to export display). See [instructions](https://code.visualstudio.com/docs/remote/ssh-tutorial)
- Python Extension Pack, Pylance, and Jupyter, python indent
- C/C++ Extension Pack, Better C++ Syntax,
- IntelliCode, Path Intellisense
- Prettier - Code formatter, Material Icon Theme
- GitLens
- Markdown All in One, markdownlint, Markdown Preview enhanced, Markdown PDF
- Settings Sync
- Better Comments, Todo Tree
- vscode-pdf
- json, edit csv
- SVG viewer
- output collorizer
- Verilog-HDL/SystemVerilog, VHDL
- Restore Terminals (to automatically setup a bunch of terminals)
  - See example config under `/export/nfs0home/ataffard/uclhc-1_workarea/PhaseII_MDT/MuonTrigger_HL-LHC/L0MDT_Ana/.vscode/terminals.json`
- ROOT File Viewers (to view ROOT file in VSCode). See [instructions](https://root.cern/blog/root-on-vscode/)

## GitLab and GitHub

Git is used for code repository. If you have a CERN account, you'll have access to [GitLab](https://gitlab.cern.ch).
Otherwise, you can create a free account on [GitHub](https://github.com)

[Quick Git tutorial](https://www.freecodecamp.org/news/learn-the-basics-of-git-in-under-10-minutes-da548267cc91/)

If you have never used Git before, have a look at the useful training and tutorial slides are located in the [LASP Trainings](https://gitlab.cern.ch/atlas-lar-be-firmware/LASP/LASP-doc/-/tree/master/Trainings) directory:

- [LASP Git Tutorial: 1. Git Basics, 2. LASP work flow, 3. GitLab Features](https://staerz.web.cern.ch/slides/LASP_Git_Tutorial.pdf)
- [LASP Git Tutorial: 4. Git Rebase, 5. Cleaning up](https://staerz.web.cern.ch/slides/LASP_Git_Rebase.pdf)
- [GIT Cheat sheet](https://staerz.web.cern.ch/slides/GitCheatSheet.txt)

## ROOT

- [ROOT homepage](https://root.cern.ch/)
- [List of Classes](https://root.cern/doc/master/annotated.html)
  - Search for the class (eg TH1 and click on the 2nd TH1 in the tree to get to [TH1 webpage](https://root.cern/doc/master/classTH1.html))
- [ROOT primer](https://root.cern/primer/)
- [ROOT manual](https://root.cern/manual/)

## Python

- [Real Python reference](https://realpython.com)
- [Python tutorial](https://www.tutorialspoint.com/python3/index.htm) (there are many more out there)
  - Covered by the basic tutorial and Advanced tutorial (classes and objects)

## Machine Learning

To get setup on ATLAS GPU machine, see these [instructions](https://gitlab.cern.ch/groups/anteater/-/wikis/General/ATLAS%20GPU%20Setup).

One can also use the [US-ATLAS analysis facility](https://usatlas.readthedocs.io/projects/af-docs/en/latest/).

- [Jupyter lab guide](https://jupyterlab.readthedocs.io/en/stable/getting_started/overview.html) and [Manual](https://jupyter.brynmawr.edu/services/public/dblank/Jupyter%20Notebook%20Users%20Manual.ipynb)
- [Markdown guide](https://www.markdownguide.org/basic-syntax/) and [Markdown cheat sheet](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
- [Panda dataframe](https://realpython.com/pandas-dataframe)
- [NumPy documentation](https://numpy.org/doc/stable/): [API](https://numpy.org/doc/stable/reference/index.html#reference)
- [Matplotlib v3.4.2](https://matplotlib.org/stable/index.html): [API](https://matplotlib.org/stable/api/index.html) and [Tutorials](https://matplotlib.org/stable/tutorials/index.html)
- [Tensorflow v2.7](https://www.tensorflow.org/): [API](https://www.tensorflow.org/versions/r2.7/api_docs/python/tf) and [Tutorials](https://www.tensorflow.org/tutorials)
- [Sklearn](https://scikit-learn.org/stable/getting_started.html): [Extension to speed code](https://intel.github.io/scikit-learn-intelex/)
- [Seaborn: statistical data visualization](https://seaborn.pydata.org/):  [API](https://seaborn.pydata.org/api.html) and [Tutorials](https://seaborn.pydata.org/tutorial.html)

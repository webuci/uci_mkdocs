# Miscellaneous

- [US ATLAS shared computing resources docs](https://usatlas.readthedocs.io/projects/af-docs/en/latest/)
- [Using containers in ATLAS with ATLASLocalRootBase/setupATLAS](https://twiki.atlas-canada.ca/bin/view/AtlasCanada/Containers)

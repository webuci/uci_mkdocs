/**
 * @Author: Anyes Taffard
 * @Date:   2022-04-12 16:15:16
 * @Last Modified by:   Anyes Taffard
 * @Last Modified time: 2022-04-12 16:15:16
 */
window.MathJax = {
  tex: {
    inlineMath: [["\\(", "\\)"]],
    displayMath: [["\\[", "\\]"]],
    processEscapes: true,
    processEnvironments: true
  },
  options: {
    ignoreHtmlClass: ".*|",
    processHtmlClass: "arithmatex"
  }
};

document$.subscribe(() => {
  MathJax.typesetPromise()
})

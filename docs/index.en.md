Collection of useful notes and documents for the UCI group.
This site is an MKdocs page, hosted via CERN webEOS server.

Note that some links in this site access private ATLAS webpages on CERN gitLab or ATLAS TWikis. You will need to have an ATLAS computing login to access these.

- [Mattermost UCI channel](https://mattermost.web.cern.ch/uci)
- [Prof. Taffard UCI website](https://sites.uci.edu/taffard/)

# Devices and Network info


## VS network diagram
<iframe frameborder="0" style="width:100%;height:659px;" src="https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1&title=188-R-024-network.drawio#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1x7RDcl31Hxdp65xTSoEEQV99KhafyI64%26export%3Ddownload"></iframe>

## Connection info

- `pcatmuontdaq01.cern.ch` acts as a gateway machine
- Any user subscribed to the l0mdt e-group has login permissions (use your CERN login credentials)
- From there, check [the correct IP](#static-ips-dhcp-names) of the board and connect
## Shelf occupancy
|Slot number|Slot address | Description|
|-|-|-|
|2| 0x84 |ATCA Switch ATC809 |
|5| 0x8A |L0MDT TP SM218 |

## Network name service, network limitations
- A dhcp client and dns service is setup with dnsmasq on pcatlmuontdaq01 acting only on the protected network
- pcatlmuontdaq01 is also the network gateway, it means that if the pc is off/offline, there is no way to communicate with the Shelf
- In case it's needed, other computers can be connected to the protected network via the ethernet ports of the ATCA switch

### Static IPs, DHCP names
The following network names are registered as rules in the network

| Device name | Network name |MAC | IP | Mode | 
|-|-|-|-|-|
|ATCA Shelf manager | shelfman | | 192.168.16.20 | Static | 
|ATCA Switch ATC809 |  | | 192.168.16.230 | Static | 
| Sm218 Zynq | sm218 | 00:50:51:ff:10:da | 192.168.16.218 | DHCP |
| Sm218 IPMC | ipmc218 | 00:80:E1:A5:89:86 | 192.168.16.118 | DHCP |



## Admin credentials
[Link](https://docs.google.com/document/d/1-uCpalC7JRxCAmgPUYCKDfZbFpqVOogVNSjP89yFLUg/edit?usp=sharing)


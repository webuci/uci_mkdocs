# L0MDT SM troubleshooting

## I2C errors when programming the FPGA (or in general)
All the software that access the mcu (e.g. pyMCU, test_stand, `config_blade.py`) should use that via the l0mdt service, ensure that it is running by doing `systemctl status l0mdt_service` and that the folder `/opt/l0mdt-sm-sw-core/bin/` is in the `$PATH` environment variable.

If you experience any issue, you can try to run the same command preceeded by `mcu_client`.
`config_blade.py`, the MCU monitoring script and few other sw will call internally mcu_client.

The following programs are **not compatible** with `mcu_client`:
1. BUTool (relevant only for the MCU serial terminal access)
2. The `test_stand` executable.
If you want to run these, make sure that all the applications that are using mcu_client are stopped and that the mcu_client is not running.




## MCU errors
If MCU reports nonsense values for the temperatures and pyMCU gives weird errors such as:
```
[root@localhost test_stand]# mcu_client /opt/mdttp-cm-demo-mcu/Software/HwTest/pyMcu/pyMcuCm.py -d /dev/ttyUL1 -c i2c_reset
[[... omitted lines ...]]
2: [    0.227406]: This is the l0mdt client
2: [     1.93994]: Resp. from command: DEBUG: ../../../opt/mdttp-cm-demo-mcu/Software/HwTest/pyMcu/hw/MdtTp_CM.py: Resetting all I2C buses.
2: [     1.97962]: Resp. from command: ERROR: ../../../opt/mdttp-cm-demo-mcu/Software/HwTest/pyMcu/hw/McuSerial.py: Incomplete response received from the MCU!
```
You can try a reset on the MCU:
```
pyMcuCm.py -c mcu_cmd_raw -p reset
```
(If the MCU hasn't booted you can do `-p r` instead of `-p reset`)

## DNS not working
You can edit `/etc/resolv.conf` and add a line like
```
nameserver 8.8.8.8
```

## Date/time not updating after a reboot
```
systemctl stop nptd
ntpd -gq
```
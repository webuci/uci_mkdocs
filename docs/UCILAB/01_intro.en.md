# Devices, Network info, access

## UCI Lab network diagram
<iframe src="https://drive.google.com/file/d/1OyH_IDIlEEbsksitHTBvxY3pPEn5k_CS/preview" width="640" height="480" allow="autoplay"></iframe>

## Connection info for UCI Lab devices
You should have an account for the Lab PC `uciatlaslab.ps.uci.edu` and be able to connect to UCInet (e.g. via UCI VPN).  
The Lab PC is connected via lightpath to internet (only approved IPs).   

As shown in the diagram all the devices are behind a ***protected network*** and no direct access is possible, `uciatlaslab` works as main gateway for operating remotely in the lab.  
[See instructions/documentation on how the firewall works](https://gitlab.cern.ch/anteater/uciatlaslab/-/blob/master/docs/firewall.md)

To access Lab devices you can ***ssh*** or connect via ***x2go*** to the Lab PC.  
Login credentials are available at the following link (restricted access).  

[Login credentials](https://docs.google.com/document/d/1PeQ744Rgdzi4pLZqoAWVOFx9YE9c940X5Dqc9Uu7uXo/edit?usp=sharing){ .md-button .md-button--primary}

### PDU Outlets
![here](../pics/PDU_settings.png)

### Backup connection
A backup connection to the PDU is enabled via UCInet, useful to turn off things if the Lab PC is impossible to reach.  
The ip address of the router is reachable within UCInet at `128.200.48.99` or `frh3142.ps.uci.edu` (tested on 26/Oct/22 and the latter doesn't work when connected directly to UCInet without VPN, for the VPN both works).  


All traffic to port `8085` of the router is forwarded to the PDU (port `80`).
This allows to connect to the PDU web interface:  
 
## Remote desktop on `uciatlaslab`
### X2GO
X2go can be used with the xfce desktop environment, you can follow the instructions [here](../Computing_resources/02_account_settings.en.md) to configure it.

Important: if you see some artifacts or black borders around the screen you can fix it by doing the following. Click on the application menu, then search for "window manager tweaks", go to the "compositor" tab and uncheck "enable compositing".

### RDP
An xrdp server is also running on `uciatlaslab`. Sometimes it may be more quick to use instead of `x2go`.

RDP uses port 3389, which is blocked on lightpath (probably by an exeternal firewall - to be checked). You can connect to it by opening an ssh tunnel
```
ssh -L 3389:localhost:3389 youruser@uciatlaslab.ps.uci.edu
```
and while this session is active, if you are on Mac/Windows one option is the Microsoft remote desktop, otherwise use remmina on windows. When you configure it, set the connection to "localhost" and use `uciatlaslab` credentials. 
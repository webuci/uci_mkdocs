# Uciatlaslab installation notes

## Vivado on Alma9
Vivado is not officially supported in Alma9. In order to install it I followed these instructions ([source](https://adaptivesupport.amd.com/s/question/0D54U00006sYwfhSAC/how-to-install-on-alma-linux-91?language=en_US)).

1. Connect from RDP (the vivado installer didn't render properly via X2go)
2. Launch the executable as root
3. In the Vivado installer program uncheck the boxes for desktop shortcut and adding to start menu. (If these boxes are checked the installer program would hang at 99%)
 
After installation completes, as root do this:
```sh
sudo dnf install -y epel-release
sudo dnf install -y ncurses-compat-libs
```

## Podman configuration for monitoring services
### Install

sudo dnf install podman-docker then follow <https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md>

### Container migration
use busybox docker image to mount podman container and copy/restore files e.g. podman run &#x2013;privileged &#x2013;rm -it -v graphite-storage:/opt/graphite/storage -v /higgs:/stuff busybox
### Systemd services
#### Pod-monitoring (main service calling the others)
```
      # pod-monitoring.service
      # autogenerated by Podman 4.6.1
      # Mon Apr 22 12:18:11 CEST 2024

      [Unit]
      Description=Podman pod-monitoring.service
      Documentation=man:podman-generate-systemd(1)
      Wants=network-online.target
      After=network-online.target
      RequiresMountsFor=/run/user/112371/containers
      Wants=container-grafana.service container-graphite.service
      Before=container-grafana.service container-graphite.service

      [Service]
      Environment=PODMAN_SYSTEMD_UNIT=%n
      Restart=on-failure
      TimeoutStopSec=70
      ExecStartPre=/usr/bin/podman pod create \
        --infra-conmon-pidfile %t/pod-monitoring.pid \
        --pod-id-file %t/pod-monitoring.pod-id \
        --exit-policy=stop \
        -p 3000:3000 \
        -p 8080:80 \
        -p 2003-2004:2003-2004 \
        -p 2023-2024:2023-2024 \
        -p 8125:8125/udp \
        -p 8126:8126 monitoring
      ExecStart=/usr/bin/podman pod start \
        --pod-id-file %t/pod-monitoring.pod-id
      ExecStop=/usr/bin/podman pod stop \
        --ignore \
        --pod-id-file %t/pod-monitoring.pod-id  \
        -t 10
      ExecStopPost=/usr/bin/podman pod rm \
        --ignore \
        -f \
        --pod-id-file %t/pod-monitoring.pod-id
      PIDFile=%t/pod-monitoring.pid
      Type=forking

      [Install]
      WantedBy=default.target

```

#### Container-service grafana
```
      # container-grafana.service
      # autogenerated by Podman 4.6.1
      # Mon Apr 22 12:18:11 CEST 2024

      [Unit]
      Description=Podman container-grafana.service
      Documentation=man:podman-generate-systemd(1)
      Wants=network-online.target
      After=network-online.target
      RequiresMountsFor=%t/containers
      BindsTo=pod-monitoring.service
      After=pod-monitoring.service

      [Service]
      Environment=PODMAN_SYSTEMD_UNIT=%n
      Restart=always
      TimeoutStopSec=70
      ExecStart=/usr/bin/podman run \
        --cidfile=%t/%n.ctr-id \
        --cgroups=no-conmon \
        --rm \
        --pod-id-file %t/pod-monitoring.pod-id \
        --sdnotify=conmon \
        --replace \
        -d \
        --name grafana \
        -v grafana-storage:/var/lib/grafana \
        -e GF_AUTH_ANONYMOUS_ENABLED=true grafana/grafana-oss
      ExecStop=/usr/bin/podman stop \
        --ignore -t 10 \
        --cidfile=%t/%n.ctr-id
      ExecStopPost=/usr/bin/podman rm \
        -f \
        --ignore -t 10 \
        --cidfile=%t/%n.ctr-id
      Type=notify
      NotifyAccess=all

      [Install]
      WantedBy=default.target
```
#### Container-service graphite
```
      # container-graphite.service
      # autogenerated by Podman 4.6.1
      # Mon Apr 22 12:18:11 CEST 2024

      [Unit]
      Description=Podman container-graphite.service
      Documentation=man:podman-generate-systemd(1)
      Wants=network-online.target
      After=network-online.target
      RequiresMountsFor=%t/containers
      BindsTo=pod-monitoring.service
      After=pod-monitoring.service

      [Service]
      Environment=PODMAN_SYSTEMD_UNIT=%n
      Restart=always
      TimeoutStopSec=70
      ExecStart=/usr/bin/podman run \
        --cidfile=%t/%n.ctr-id \
        --cgroups=no-conmon \
        --rm \
        --pod-id-file %t/pod-monitoring.pod-id \
        --sdnotify=conmon \
        --replace \
        -d \
        --name graphite \
        -v graphite-conf:/opt/graphite/conf \
        -v graphite-storage:/opt/graphite/storage \
        -v graphite-custom:/opt/graphite/webapp/graphite/functions/custom \
        -v graphite-nginx:/etc/nginx \
        -v graphite-config:/opt/statsd/config \
        -v graphite-logrotate:/etc/logrotate.d \
        -v graphite-log:/var/log \
        -v graphite-redis:/var/lib/redis \
        graphiteapp/graphite-statsd
      ExecStop=/usr/bin/podman stop \
        --ignore -t 10 \
        --cidfile=%t/%n.ctr-id
      ExecStopPost=/usr/bin/podman rm \
        -f \
        --ignore -t 10 \
        --cidfile=%t/%n.ctr-id
      Type=notify
      NotifyAccess=all

      [Install]
      WantedBy=default.target
```



### #Steps to run the above containers at startup [do this as root]

1.  Copy the container services
2.  Tell SELinux that these are fine /sbin/restorecon -v /etc/systemd/system/container-gra\* /sbin/restorecon -v /etc/systemd/system/pod-moni\*
3.  systemctl daemon-reload
4.  podman pull grafana/grafana-oss podman pull graphiteapp/graphite-statsd
5.  systemctl enable pod-monitoring
6.  systemctl start pod-monitoring



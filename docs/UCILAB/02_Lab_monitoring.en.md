# Lab monitoring
[Link to GitLab repo](https://gitlab.cern.ch/anteater/uciatlaslab)

## Monitoring framework
### Grafana
To connect to grafana you need to forward port 3000 of uciatlaslab to your localhost, one way to do that is to use the option `-L` of `ssh`  
```
ssh -L 3000:localhost:3000 yourusername@uciatlaslab.ps.uci.edu
```
While the process is running the grafana webpage will be accessible via your browser at the following address
[http://localhost:3000](http://localhost:3000)

Login credentials are available [here](https://docs.google.com/document/d/1PeQ744Rgdzi4pLZqoAWVOFx9YE9c940X5Dqc9Uu7uXo/edit?usp=sharing).  

!!! note
	With the `-L` option `ssh` will login to the remote machine and keep the port forwarded until the connection is up. You can optionally use a keypair authentication method, combined with option `-N` and with the `&` at the end to send this job to background. 

!!! note
	Grafana is currently hosted on a docker container. It will start automatically with the Docker service. In case there are issues with it (can happen after a Lab PC reboot) try `docker restart grafana`.



### Alarms
Automatic alarms that managed within grafana and are sent to the _LabMonitoring_ mattermost channel. 

A brief list of instruction on how to react to alarms is reported in the [alarm help](99_Alarm_help.en.md)

## Graphite database
A graphite databased is used as backend database to store all the info recorded for grafana. It represent a convenient way to store time-ordered data.  

As done with Grafana, this is currently hosted on a docker container. It will start automatically with the Docker service. In case there are issues with it (can happen after a Lab PC reboot) try `docker restart graphite`.

!!! question "How does it work? Graphite APIs"
    An http API allows to insert metrics into the database, e.g. by doing ```echo "foo.bar 1 `date +%s`" | nc localhost 2003```  	
	you will get an entry under /foo/bar corresponding to 1. Port `2003` is listened by the data-recording API.

	Database entries can be browsed, after forwarding port `8080` of the Lab PC to your computer, at the following url:  
	[http://localhost:8080](http://localhost:8080)


	See additional information [here](https://graphiteapp.org/#gettingStarted)  


## Details on monitoring software

Currently we monitor PDU, UMS and L0MDT ATCA Crate.  
[Gitlab repo for code and Lab PC configuration](https://gitlab.cern.ch/anteater/uciatlaslab)

### Overview

Shelf manager monitoring software is based on the software in use at BU [link to gitlab](https://gitlab.com/BU-EDF/shelf-tools), written in C++ and using IPMI raw commands to get info from the shelf (and optionally boards connected to it).  
PDU and UMS monitoring are stand-alone and based on python stand-alone scripts that run as daemons on uciatlaslab (see below).

At the moment all of these push data to the graphite database, which is then read by grafana.  
The three executables run as services on the Lab PC, services are enabled and are set to start automatically when the system boots.

!!! question "How can I start/stop these services?"
	```sudo systemctl start monitoring_shelf```  
	```sudo systemctl start monitoring_pdu```  
	```sudo systemctl start monitoring_ums```  
	stopping services can be done by replacing `start` with `stop`.

### Details on Shelf manager monitoring 
Nothing more than the config script that must be used in order to make the BU monitoring work with our current configuration.  
[Link to config script in our gitlab page](https://gitlab.cern.ch/anteater/uciatlaslab/-/blob/master/monitoring/uci_graphite_monitor.config)

### Details on UMS monitoring
The UMS noise-cancelling rack is controlled by a raspberry pi with a custom OS running.  
Its OS does not allow any kind of access to temperatures and fan speed from the command line (and neither a ssh connection).

The only available way to access these values is from a webpage (reachable at the ip of the rPi).  

The current monitoring script [link](https://gitlab.cern.ch/anteater/uciatlaslab/-/blob/master/monitoring/UMS/UMSscraper.py) uses a web-scraping python module to run a firefox session and constantly forward the values to the graphite db. This is necessary as the page is not a simple html webpage, but is reachable after logging in via the web form.


### Additional details on PDU monitoring
The PDU in our lab supports SNMPv2 transactions to read its status (and monitoring values). This is used in our grafana page to monitor the status of the outlets.

[Link to code in gitlab](https://gitlab.cern.ch/anteater/uciatlaslab/-/blob/master/monitoring/PDU/PDUmonitoring.py)

A snmp python module (pysnmp) is used to handle the transaction.


### Monitoring of the eftrk01 server
The Event Filter Tracking server has an IPMC controller, but for simplicity its readout is done with a custom script (and not the code from BU that is more focused on ATCA crates).

[Link to code in gitlab](https://gitlab.cern.ch/anteater/uciatlaslab/-/blob/master/monitoring/EFTrk/EFTrkMonitoring.py)

The code simply makes a system call to `ipmitool` and parses the output.


### MCU monitoring on Apollo blades (summer 2023)
The MCU on the board is not reportin all the CM measurements to the IPMC (July 2023 - not reporting anything at the moment).  
This section explains how to overcome this issue.

Requirements:
1. IPMC firmware must be taken from the branch atlas-temp-sensors
2. l0mdt_service must run (better to do it with systemctl)
3. everything that is interfacing with the MCU is called via `mcu_client`

[This script](https://gitlab.cern.ch/anteater/uciatlaslab/-/blob/master/monitoring/L0MDT_CM_MCU/CM_monitoring.py) runs [pyMCU](https://github.com/dacieri/mdttp-cm-demo-mcu/tree/main/Software/HwTest/pyMcu) to get information from the CM MCU via i2c.

### How to upgrade grafana/graphite container
If you need to upgrade any software running in docker ensure the following:
 * The container is started with a bind-mount to a local directory or to a docker volume
 * All the settings are saved in the mounted directory or volume
 * You know exatcly what you are doing
If the three above are true, then you simply need to remove and restart the container after doing docker pull
Let's refer to the case of the grafana image.

 ```
 docker pull grafana/grafana-oss
 docker container stop grafana
 docker container rm grafana
 source /home/ilongari/monitoring/graphite/RunGrafana.sh
 ```

More infos are available [here](https://grafana.com/docs/grafana/latest/upgrade-guide/).
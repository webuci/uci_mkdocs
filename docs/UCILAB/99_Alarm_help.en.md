# Alarm help 
Instructions for how to react to alarms   

## Silencing an alarm
Go to the grafana webpage, and go to Alerting-> Silences, on the left handside menu. 
Create a new silence rule.

## DatasourceNoData alarms
Grafana is complaining that no data is being reported. Check if the scripts are running by calling `systemctl` on uciatlaslab, the output should be similar (showing `active running`).

```
[ilongari:~]$ systemctl | grep monitoring
  monitoring_eftrk.service                                                                                        loaded active running   Event Filter Tracking Server monitoring service
  monitoring_pdu.service                                                                                          loaded active running   PDU monitoring service
  monitoring_shelf.service                                                                                        loaded active running   Shelf manager monitoring service
  monitoring_ums.service                                                                                          loaded active running   UMS noise-cancelling rack monitoring service
```

If the service is running maybe the data is not being reported. For example, it may happen that the MDT-TP blade is disconnected (and the monitoring_shelf service doesn't know it) and grafana complains that no data is available.

## Fan speed alarms
Alerts are set on the fan speed of the Nois-cancelling rack, of the ATCA crate and of the EFTracking server. These will trigger if the fan speed goes below 100 RPM above 10K RPM for more than 2 minutes.

## Temperature alarms
For the ATCA blades: everything should be handled by the Shelf Manager. If a temperature value is listed in the IPMC, then it's managed. 



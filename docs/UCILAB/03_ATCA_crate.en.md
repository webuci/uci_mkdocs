# ATCA Crate Notes

## General
Our current setup has a Pigeon Point Shelf Manager MODEL-NAME MODEL-NUMBER.  

Access is possible via `uciatlaslab` and `uceftrk01`. The lab network map and connection details are in [the introduction page](01_intro.en.md).

Below I am reporting some (mostly uncategorized) commands useful for managing the ATCA shelf, as well as information on the configuration of the F125 switch.


## Address map

| Logical addresss | Physical slot | Device name   |
|------------------|---------------|---------------|
| 0x20             |               | Shelf Manager |
| 0x82             | 1             | Not used      |
| 0x84             | 2             | F-125 switch  |
| 0x86             | 3             | Not used      |
| 0x88             | 4             | Apollo SM-219 |
| 0x8a             | 5             | Not used      |
| 0x8c             | 6             | Not used      |
| 0x8e             | 7             | Not used      |

more info obtainable with the following command (from the ShM prompt).
```
clia shelf at 
```

## Access via IPMI (ipmitool)

[ipmitool](https://github.com/ipmitool/ipmitool.git) is available on `uciatlaslab` at `~/ilongari/.local/ipmitool/bin/ipmitool`.  Consider adding this folder to your PATH, or add it as an alias in your `.profile`.

The following command prints a helper window with possible commands that can be sent to the device via ipmi.  
Note that the IP address, username (with the `-U` option) and password (`-P` option) must be specified correctly.
```
ipmitool -H 192.168.1.200 -P "" help
```


### Sensor listing
The following command prints all the IPMI registers available for the blade at address 0x88.  
Note that `-t 0x88` tells the Shelf to report the sensor listings for the board at the logical address 0x88 (L0mdttp board in this case).
``` 
$ ipmitool -H 192.168.1.200 -P "" sensor -t 0x88
Hot Swap Carrier | 0x0        | discrete   | 0x1000| na        | na        | na        | na        | na        | na
IPMB-0 Sensor    | 0x88       | discrete   | 0x0800| na        | na        | na        | na        | na        | na
SM Top Temperatu | 30.000     | degrees C  | ok    | na        | na        | na        | 38.000    | 40.000    | 50.000
SM Mid Temperatu | 29.000     | degrees C  | ok    | na        | na        | na        | 38.000    | 40.000    | 50.000
SM Bot Temperatu | 28.000     | degrees C  | ok    | na        | na        | na        | 38.000    | 40.000    | 50.000
CM FPGA1 Tempera | na         |            | na    | na        | na        | na        | 50.000    | 65.000    | 80.000
CM FPGA2 Tempera | na         |            | na    | na        | na        | na        | 50.000    | 65.000    | 80.000
CM Firefly Max T | na         |            | na    | na        | na        | na        | 44.000    | 50.000    | 55.000
CM Regulator Max | na         |            | na    | na        | na        | na        | 45.000    | 55.000    | 65.000
CM MCU Temperatu | na         |            | na    | na        | na        | na        | 45.000    | 55.000    | 65.000
PIM400 Temperatu | 32.320     | degrees C  | ok    | na        | na        | na        | 59.760    | 69.560    | 49.960
PIM400 Current   | 0.282      | Amps       | ok    | na        | na        | na        | 6.956     | 7.990     | 8.930
PIM400 -48V_A    | 47.775     | Volts      | ok    | na        | na        | na        | 59.800    | 65.000    | 69.875
PIM400 -48V_B    | 47.775     | Volts      | ok    | na        | na        | na        | 59.800    | 65.000    | 69.875
IPMC IP Byte 0   | 192.000    | unspecified | ok    | na        | na        | na        | na        | na        | na
IPMC IP Byte 1   | 168.000    | unspecified | ok    | na        | na        | na        | na        | na        | na
IPMC IP Byte 2   | 21.000     | unspecified | ok    | na        | na        | na        | na        | na        | na
IPMC IP Byte 3   | 65.000     | unspecified | ok    | na        | na        | na        | na        | na        | na
```



This other command prints the Sensor Data Repository of the Shelf. 
```
$ ipmitool -H 192.168.1.200 -P ""  -t 0x20 sdr elist all
ShMM_700         | 00h | ok  | 240.96 | Dynamic MC @ 10h
PPS BMC          | 00h | ok  | 240.1 | Dynamic MC @ 20h
ShelfFRU1        | 00h | ns  | 242.96 | Logical FRU @01h
ShelfFRU2        | 00h | ns  | 242.97 | Logical FRU @02h
Dc2Dc_1          | 00h | ns  | 20.96 | Logical FRU @05h
Dc2Dc_2          | 00h | ns  | 20.97 | Logical FRU @06h
FanDriver1       | 00h | ns  | 144.96 | Logical FRU @07h
FanDriver2       | 00h | ns  | 144.97 | Logical FRU @08h
FanTray1_SP      | 00h | ns  | 30.96 | Logical FRU @09h
FanTray2_SP      | 00h | ns  | 30.98 | Logical FRU @0Ah
FrontPanel       | 00h | ns  | 12.96 | Logical FRU @0Bh
RJ45             | 00h | ns  | 13.97 | Logical FRU @0Dh
PSU2             | 00h | ns  | 10.97 | Logical FRU @0Fh
PSU3             | 00h | ns  | 10.98 | Logical FRU @10h
PSU4             | 00h | ns  | 10.99 | Logical FRU @11h
PSU5             | 00h | ns  | 10.100 | Logical FRU @12h
ATCA-F125        | 00h | ok  | 160.96 | Dynamic MC @ 84h
APOLLO-OPENIPMC  | 00h | ok  | 160.96 | Static MC @ 88h
```

## Access via ssh 
The shelf manager is accessible via ssh.  
Shelf-specific commands can be issued with the `clia` program. You can either enter the `clia` prompt just by calling the program from the ShM shell, or you can directly issue commands as argument of `clia`.  

Few examples are reported below. Others can be seen with `clia help`.  

### Deactivate/Activate a slot 
```
clia deactivate board <board slot>
clia activate board <board slot>
```

### List all the FRUs
```
clia fru
```

### List all the slots
```
clia shelf at
```

## F125 switch configuration
Reporting the configuration used to enable traffic between `uciatlaslab` and ATCA blades.  
This example considers the current (Mar 2023) configuration, where the only other blade is the MDT-TP prototyle "SM-101".  

Requirements:
* F125 has two switches capable of different speed: the base switch (max 1G) and the fabric switch (max 10G)
* SM101 connects to the base switch
* F125 is connected to the lab router via ETH1 interface on the front panel (needs SFP+ to ehternet adapter)

Steps to allow traffic between SM101 and Lab router via base channel
* Disable dhcp server on F125 (we use NETGEAR Router as DHCP server), use `service dhcpd stop`
* Enable interface corresponding to slot used by SM-101 (currently slot 4, so interface ge2 for the base channel)
* Enable interface xe3 (corresponding to ETH1) and assign it to VLAN 22 
 
 
Commands on the imish shell:
```
en
conf t
interface ge2
no shutdown
interface xe3
switchport hybrid vlan 22
no shutdown
exit
exit
```

If everything is correct and SM101 net interface is set to obtain its address via dhcp, then you should see it getting an IP from the router. You can monitor the syslog for dhcp client messages. To force dhcp client to restart you can do `dhclient -r` to flush and `dhclient` to start it again (need to close with `CTRL+C`).  


Currently a setup script for this is available on F125 at `/root/config/setup_f125_vlans`. Same script is also copied on [uciatlaslab git repo](https://gitlab.cern.ch/anteater/uciatlaslab/).  

Some documentation on the F125 configuration:
* [Notes from the FTK people on the ATCA F125 Switch](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/FastTrackerATCAF125)
* BBS on ATCA-F125 with SRstackware (Artesyn - 6806800U61A)
 
 

# Upgrading FW or SW on the SM ATCA Blade 

## SM Upgrade 

### SM board replacement
[See notes](https://docs.google.com/document/d/1FMH67ZqPWVrRSG5yUzvD0Gh8YDMBxz5AO70XIoh05-w/edit)


### SM kernel image
[Instructions to build SM kernel image](https://apollo-lhc.gitlab.io/SM_Linux_Build/SM_Build/)

This repository will be upgraded to use Hog and will generate CI archives with tarballs.  
Note: This is not required for SM rev2 as no recent changes have been done with the boot image (080923)
     

### SM Root File system

Pick up tarball for Root File System from CI archive of [soc-os](https://gitlab.com/apollo-lhc/soc-os/-/tree/develop?ref_type=heads) repo.

[BU notes for building filesystem]( https://apollo-lhc.gitlab.io/SM_Linux_Build/SM_Build/#building-the-filesystem)

### SM Boot - Configuring SD card, eMMC Memory
[Boot via SD Card](https://apollo-lhc.gitlab.io/Service-Module/service-module/#sdcard)

SM v2- Boot via eMMC

Partition eMMC or SD card to have BOOT and rootfile system setup. Set the boot mode of Zynq to correct value.

Use bootmode command in IPMC to set up Boot Mode of Zynq 7-series. See [here](https://apollo-lhc.gitlab.io/IPMC/01-ipmc-terminal/#status-commands). 

A restart needs to be done in IPMC terminal for the changes to take effect.

```
Note: For eMMC boot, the bootmode command needs to be set to 0 for Zynq USP, 1 for Zynq 7-series.
```


[apollo_updater](https://gitlab.com/apollo-lhc/soc-tools/apollo-updater) tool can be used to install the boot and fs tarballs.

When moving from centos to Alma Linux, it would be straighforward to 

 1. First boot from SD card, 
 2. Mount eMMC device
 3. Delete FS in eMMC device
 4. untar FS tarball in eMMC device
 5. Change boot mode to eMMC device
 6. Re-boot blade

### SM Ethernet setup

Ethernet MAC address setup by IPMC. IP address obtained via DHCP.  
BUTools command "read *MAC*" lists MAC address from IPMC for eth0 and eth1.  
If MAC address changes then IP will also change. Look at file/fw/SM/eth0_mac.dat and make sure MAC address match the address in ifconfig command.  

[BU notes on zynq-eth-interface](https://apollo-lhc.gitlab.io/Service-Module/zynq-eth-interface/)

### Software Installation

#### pyMCU
1. `python3 -m pip install pySerial`
2. Clone these two repos under `/opt/`
    - For CM demonstrator: https://github.com/dacieri/mdttp-cm-demo-mcu/tree/main
    - For CM prototype: https://github.com/dacieri/mdttp-cm-proto-mcu

It might be usefult to add the path containing the pyMCUcm.py to the `PATH`, e.g. `export PATH=$PATH:/opt/mdttp-cm-demo-mcu/Software/HwTest/pyMcu/`

#### L0MDT Services (l0mdt_sm_sw_core)
This package depends on pyMCU for some functionalities.
Follow the installation instructions in [l0mdt-sm-sw-core repo](https://gitlab.cern.ch/atlas-tdaq-phase2-l0mdt-electronics/l0mdt-sm-software/l0mdt-sm-sw-core).

It is important to clone this under `/opt/` and to add `/opt/l0mdt-sm-sw-core/bin/` to the `PATH` (in order to have the `mcu_client` command available). 


###L0MDT Software, Firmware

####l0mdt_sm_sw_core 

 Follow instructions in [repository](https://gitlab.cern.ch/atlas-tdaq-phase2-l0mdt-electronics/l0mdt-sm-software/l0mdt-sm-sw-core)

 
 ***For CM demonstrator***
```
 make ISDEMO=1

 Add bin to PATH
 PATH=$PATH:/opt/l0mdt-sm-sw-core/bin

```


####test_stand 

Follow instructions in [repository](https://gitlab.cern.ch/atlas-tdaq-phase2-l0mdt-electronics/test_stand)

## Upgrade of microcontroller on the CM
### MCU Firmware
Build firmware using mdttp-cm-demo-mcu MCU repository(WHICH BRANCH??) on CentOS machine. Copy binary to SM board, use sflash that comes with mdttp-cm-demo-mcu to flash the MCU

```
In case the fw gets corrupted, it can be flashed again because the bootldr is write protected and can’t be wrecked (we hope).  
In BUTools, reset the MCU with the commands
write CM.CM_1.CTRL.ENABLE_UC 0
write CM.CM_1.CTRL.ENABLE_UC 1
This starts a 5-second countdown on the MCU, quickly get to the console and interrupt it with
uart_term CM_1
<any character> (to stop the countdown and enter the bootldr dialog)
f (to force fw download)
ctl-] (to exit uart_term terminal)
Then quit BUTools and run sflash from
/opt/mdttp-cm-demo-mcu/Software/TivaWare/SW-TM4C-2.2.0.295/tools/sflash:

Make sure that no minicom session or other utility is using the uart interface.
Run sflash command- 
./sflash -c /dev/ttyUL1 -p 0x4000 -b 115200 -d -s 252 cm_mcu_hwtest.bin 


If sflash fails or hangs, check if the bootloader is intact by resetting the MCU via BUTools and restart the sflash command

Running "make sflash" from MCU repo erases firmware at 0x4000 and copies the firmware. This worked on SM219
```

Notes for recovering from MCU locked state can be found [here](https://github.com/apollo-lhc/cm_mcu/wiki/Recovering-a-locked-TM4C)

### IPMC Firmware
Upgrade can be done via ipmitool. Use firmware tarball from [CI artifact using branch feature/atlas_temp_sensors branch](https://gitlab.com/BU-EDF/openipmc-fw/-/pipelines?page=1&scope=all&ref=feature%2Fatlas-temp-sensors). 

Download the artifacts and copy the content of the repo in a directory on a server that has ipmitool installed. 

[Remote Upgrade via ipmitool](https://apollo-lhc.gitlab.io/IPMC/05-ipmc-firmware-upgrade/#remotely-through-ipmi-tool)

[Firmware Build/Upgrade](https://apollo-lhc.gitlab.io/IPMC/05-ipmc-firmware-upgrade/)

### Clock Synthesizer 
L0MDT and test_stand software (l0mdt-sm-sw-core) uses pyMcuCm.py in [mdttp-cm-demo-mcu](https://github.com/mppmu/mdttp-cm-demo-mcu/tree/main/Software/HwTest/pyMcu) to configure clocks.

use mcu_client to check if clocks are locked

```
# On Demonstrator 
mcu_client /opt/mdttp-cm-demo-mcu/Software/HwTest/pyMcu/pyMcuCm.py -c clk_status_regs

# On Prototype 
mcu_client /opt/mdttp-cm-demo-mcu/Software/HwTest/pyMcu/pyMcuCm.py -c clk_status
```


## CM  Upgrade

### Building CM Tarball
TBA

### Configuring the CM Blade
See John's notes (config_blade_demo.py, config_blade_proto.py). Does this need to be manually installed on SM or is it packaged into the RFS build?

#Repositories

[MCU Prototype](https://github.com/dacieri/mdttp-cm-proto-mcu)

[MCU Demonstrator](https://github.com/mppmu/mdttp-cm-demo-mcu)

[IPMC](https://gitlab.com/BU-EDF/openipmc-fw/-/tree/feature/atlas-temp-sensors?ref_type=heads)

[SM Kernel Source Code](https://github.com/apollo-lhc/SM_ZYNQ_FW)

[SM SoC FileSystem](https://gitlab.com/apollo-lhc/soc-os)

[SM soc-tools] (https://gitlab.com/apollo-lhc/soc-tools) - apollo_updater, bootup-daemon, netconfig-daemon

[CM Firmware](https://gitlab.cern.ch/atlas-tdaq-phase2-l0mdt-electronics/l0mdt-hdl-design)

[L0MDT Software](https://gitlab.cern.ch/atlas-tdaq-phase2-l0mdt-electronics/l0mdt-sm-software/l0mdt-sm-sw-core)

[test_stand] (https://gitlab.cern.ch/atlas-tdaq-phase2-l0mdt-electronics/test_stand)



# Useful links
[Johns Notes](https://docs.google.com/document/d/1Z9S8t9TaZgzNo6Y2raiU0UC9wk1d7_b3fBgsE2yeAT8/edit#heading=h.tnnxrpv683hq)

[Apollo LHC Documentation](https://apollo-lhc.gitlab.io/) - IPMC, SM


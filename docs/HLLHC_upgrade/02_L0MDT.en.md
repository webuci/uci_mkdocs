# L0MDT

- [MDTTP Web Page](https://mdttp.docs.cern.ch)
- Mailing Lists
  - `atlas-tdaq-phase2-L0MDT`
  - `atlas-tdaq-phase2-l0mdt-electronics`
  - `atlas-tdaq-phase2-muon`
  - `atlas-muon-phase2-mdt-electronics` (Muon FE Electronics)  

## Indico

- [L0MDT Working Group & Joint Muon-TDAQ Meetings](https://indico.cern.ch/category/5272/)
- [UCI L0MDT Meetings](https://indico.cern.ch/category/1481/)

## GitLab Repositories

- [L0MDT Firmware](https://gitlab.cern.ch/atlas-tdaq-phase2-l0mdt-electronics)
- [Common Offline Framework](https://gitlab.cern.ch/atlas_hllhc_muon_trigger)
- [Documentation](https://gitlab.cern.ch/atlas-hllhc-muon-mdt-trigger-doc)
- [BU repository](http://sites.bu.edu/edf/)
  - [Eric’s Talks](http://gauss.bu.edu/svn/atlas-phase-2-muon-upgrade/Meetings/)
  - [BU projects List links to svn repo](http://ohm.bu.edu/trac/edf/wiki/ProjectList)
  - [BU project list to svn repo](https://gauss.bu.edu/redmine/projects)

## CERNBox, NextCloud & other Webpages

- [MPI NextCloud](https://nextcloud.mpp.mpg.de/nextcloud/index.php/s/wa3LFPS8rW7dAXD)
- [Markus Fras L0MDT](https://cernbox.cern.ch/index.php/apps/files/?dir=/__myshares/L0MDT%20(id%3A204843)&)
- [L0MDT Common Analysis FrameWork CERNbox](https://cernbox.cern.ch/index.php/apps/files/?dir=/__myshares/CommonAnalysisFramework%20(id%3A258294)&)
- [Muon Upgrade Studies](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MuonUpgradeStudies)

## UCI L0MDT Docs

- [UCI L0MDT GIT projects](https://gitlab.cern.ch/atlas_hllhc_uci_mdtTrigger)
- [L0MDT Google Drive](https://drive.google.com/drive/folders/1MVpHl31M3jbBVP6hd9heI45G_84s409J?usp=sharing)
- [L0MDT Dropbox (pwd required)](https://www.dropbox.com/sh/7tsmg5kqauvqg87/AAA7hP2k96AzREr8OJ99JvYKa?dl=0)
- [Alex’s google doc datasets](https://docs.google.com/spreadsheets/d/15tYI1HHsPaXV54MS37RqZlJNQuXKpX0rxXNnSHxl8AQ/edit#gid=1526379585)
- [L0MDT Test Bench Documentation](https://atlas-uci.web.cern.ch/atlas-uci/uci-doc/l0mdt-test-bench.html)

## Getting started with L0MDT framework

The [L0MDT group git repo](https://gitlab.cern.ch/atlas_hllhc_muon_trigger) contrains the instruction to produce 'muTrigNt' from ESD and run the L0Muon algortihm over 'muTrigNt' to produce various ntuples.

On the UCI servers, the code should reside either under your workarea in `/uclhc-1` or `/uclhc-2`  or under uclhc-2 ssd drive (`/quick` or `/fast/`). Make sure to create the appropriate directory with your username first. Note that the space on the ssd drive is limited and you should refrain from keeping large or too many files.


- Make muTrigNt (muTrigNt_write): Follow instructions under this [README](https://gitlab.cern.ch/atlas_hllhc_muon_trigger/muTrigNt_write)
  - Note this package is still based on the release use for the TDR (20.20) and will need to be updated to release 23.
- Read muTrigNt to make ntuples (muTrigNt_read): Follow the instruction under this [README](https://gitlab.cern.ch/atlas_hllhc_muon_trigger/muTrigNt_read) to setup the packages
  - Make sure that you use the last tag generate (see under git which tag is the latest: eg v1.0.116). You can update to the latest tag by running:
  ```bash
  source/scripts
  ./gitUpdate2Tag.sh v1.0.116
  ```
  With the code compile, you can run various executables: 
  - `RunL0MuonTriggerLooper`: run the L0Muon algorithm sequence. Does not produce any output. Mainly used to debug the algorithm looking at the log.
    - Looking through `L0MDTLooper/Root/L0MuonTriggerLooper.cxx` is a good place to start to follow what algorithms are run and in which sequence.
  - `PerfNtMaker`: Produces the PerfNt ntuple use to measure the performance of each algorithm
  - `TVMaker`: Produces the TV ntuple used to create the bitstream file to test the MDTTP firmware.
  - `ptParamCalc`: Produces the pt parametrization
  - `mkvalnt`: produces the validation ntuple to check the parametrization and make performance plots
  
  To run on these executable locally, you can have a look in `/quick/ataffard/PhaseII_MDT/MuonTrigger_HL-LHC/L0MDT_TestVectors/run/setInputs.sh` to find input files and instructions.
  To make a ntuple production using HTCondor, have a look in (Do not run a full production, the output is almost 1TB!):
  - `/quick/ataffard/PhaseII_MDT/MuonTrigger_HL-LHC/L0MDT_TestVectors/run/submitCondor.sh`
  - `/quick/ataffard/PhaseII_MDT/MuonTrigger_HL-LHC/L0MDT_TestVectors/run/CHECK_JOBS.txt`

A schematic of the workflow can be found in the UCI gDrive under [workflow_ptParam](https://drive.google.com/file/d/1hcNPTDLNwOc19Dnsh82Kvma0aghNzg4J/view?usp=share_link)

The [UCI L0MDT repo](https://gitlab.cern.ch/atlas_hllhc_uci_mdtTrigger) contains various packages. Some of then have now been migrated under the L0MDT group repo.
The main packages are:
- [L0MDTPerfAna](https://gitlab.cern.ch/atlas_hllhc_uci_mdtTrigger/L0MDTPerfAna): produces the perfomance plots for the algorithm. Follow the README file to use it.
  - The input files are the ntuples produced by `PerfNtMaker`
- [l0ml](https://gitlab.cern.ch/atlas_hllhc_uci_mdtTrigger/l0ml): Package to run regression to extract parametrization
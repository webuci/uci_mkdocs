# Introduction

The UCI group in involved in two HL-LHC trigger upgrade projects:

- [L0MDT](02_L0MDT.en.md)
- [EFTracking](03_EFTrk.en.md)

The upgrade [TDAQ HL-LHC](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/TDAQPhase2UpgradeProject) project page.

Useful mailing list to join:

- `atlas-tdaq-firmware-design`
- `atlas-tdaq-phase2-upgrade`
- `atlas-upgrade-readout`
- `atlas-muon-upgrade-phase2`
- `atlas-upgrade-tdaq-eventselection`
  
## CERN Divers

- [CERN ATCA procurement](https://espace.cern.ch/ph-dep-ESE-BE-ATCAEvaluationProject/Procurement/SitePages/Home.aspx)
- [Procurement Supplier Portal](https://procurement.web.cern.ch/suppliers-portal)
- [CERN Store catalogue](https://edh.cern.ch/edhcat/Browser?command=changeTopNode&argument=1&top=1&objid=%24%24EDH7geoj0lp6&showAdvanced=&scem=&keywords=)

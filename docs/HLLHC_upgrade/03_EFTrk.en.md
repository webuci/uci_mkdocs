# EFTrack

- [EFTracking Twiki](https://twiki.cern.ch/twiki/bin/view/Atlas/EFTracking)
- [EFTracking Jira](https://its.cern.ch/jira/projects/EFTRACK/summary)

Mailing list:

- `atlas-tdaq-phase2-eftracking`
- `atlas-usa-eftracking`

## Useful commands
- XBUTIL command shows kernel in card → unable to program new version of kernel if host program did not exit gracefully → reset kernel via `xbutil reset --d 0000:c1:00.1`

## Presentations with useful information from the May 2023 Tracking Workshop
- [Tracking (challenges) overview](https://indico.cern.ch/event/1258574/contributions/5389749/attachments/2647186/4582440/TrackingChallenges_VMMCAIRO_15May2023.pdf)
- [Tracking performance metrics](https://indico.cern.ch/event/1258574/contributions/5389760/attachments/2647797/4583746/Tracking%20Performance%20-%20Workshop-2.pdf)
- [Overview from Elliot on FPGAs for Tracking](https://indico.cern.ch/event/1258574/contributions/5389773/attachments/2647799/4583958/FPGAs%20for%20tracking.pdf)
- [Overview from Ben on GPU hardware and some links to other experiments using GPUs for tracking](https://indico.cern.ch/event/1258574/contributions/5389773/attachments/2647799/4583603/GPUhardware.pdf)

## UCI EFTrk Docs

- [UCI EFTrk GIT](https://gitlab.cern.ch/uci-eftrk)
- [EFTrk GoogleDrive](https://drive.google.com/drive/folders/1EmJCULqCe24khRz0fbUV1JnBYdhL-nvz?usp=sharing)
- [EFTrk UCI Dropbox (Pwd required)](https://www.dropbox.com/sh/ifjb9ymo5n0agos/AAB6VzSaFUWKA8_BEZ6KwdKWa?dl=0)
